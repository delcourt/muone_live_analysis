#include "TreeReader.h"
#include "FastReader.h"
#include <iostream>

using namespace std;



FastReader::FastReader(string fName_){
    
    _ff = new TFile(fName_.c_str());
    _tt = (TTree *) _ff->Get("fast_event");
    event_counter = 0;
    n_entries = _tt->GetEntries();
    me_ref = &me; 
    _tt->SetBranchAddress("fast_event"     ,&me_ref);
}




FastReader::~FastReader(){
    _ff->Close();
    delete _ff;
}

void FastReader::peekLastEvent(){
    _tt->GetEntry(n_entries-1);
}

void FastReader::peekFirstEvent(){
    _tt->GetEntry(0);
}

int FastReader::getEvent(int event_number){
    if (event_number <0){
        if (event_counter < n_entries){
            _tt->GetEntry(event_counter);
            event_counter++;
            return 1;
        }
        else{
            return 0;
        }
    }
    else{
        if ((uint)event_number < n_entries){
            event_counter = event_number;
            _tt->GetEntry(event_number);
            return 1;
        }
        else{
            return 0;
        }
    }
}

uint64_t FastReader::getEntries(){
    return n_entries;
}
string   FastReader::getProgress(){
    return "Processed "+to_string(event_counter) + " / "+ to_string(n_entries)+" : ~" + to_string((100*event_counter)/n_entries)+"%";

}
