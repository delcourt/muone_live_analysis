#include "HistManager.h"
#include "TFile.h"
#include "TH1F.h"
#include "TF1.h"
#include <iostream>
using namespace std;

EffContainer2D::EffContainer2D(std::string name, int n_bins_x, float x_0, float x_1,int n_bins_y, float y_0, float y_1){
    teff = new TEfficiency((name+"_eff").c_str(), (name+"_eff").c_str(), n_bins_x, x_0, x_1, n_bins_y, y_0, y_1);
}        
EffContainer2D::~EffContainer2D(){delete teff;}
void EffContainer2D::Fill(float x,float y, bool matched){teff->Fill(matched, x, y);}
void EffContainer2D::Write(){teff->Write(teff->GetTitle());};




EffContainer::EffContainer(std::string name, int n_bins, float x_0, float x_1){
    numerator = new TH1I((name+"_num").c_str(), (name+"_num").c_str(), n_bins, x_0, x_1);
    denominator = new TH1I((name+"_den").c_str(), (name+"_den").c_str(), n_bins, x_0, x_1);
    ratio = new TH1F(name.c_str(), name.c_str(), n_bins, x_0, x_1);
    n_entries_processed = 0; 
    start=x_0; 
    stop=x_1;
    teff = new TEfficiency((name+"_eff").c_str(), (name+"_eff").c_str(), n_bins, x_0, x_1);
}

EffContainer::~EffContainer(){
    delete numerator;
    delete denominator;
    delete ratio;
    delete teff;
}

void EffContainer::Fill(float x,bool matched){
    denominator->Fill(x);
    if (matched)
        numerator->Fill(x);
    teff->Fill(matched,x);

}

std::pair <float,float> EffContainer::GetEff(){
        ComputeRatio();
        TF1  *fit_fn = new TF1("fit_fn","pol0",start,stop);
        ratio->Fit(fit_fn,"WL0","",start,stop);
        pair <float,float> result;
        result.first = fit_fn->GetParameter(0);
        result.second = fit_fn->GetParError(0);
        delete fit_fn;
        return result;
}

std::vector < std::pair <float,float> > EffContainer::GetCbcEff(){
        ComputeRatio();
        cerr<<"Not implemented"<<endl;
        return std::vector < std::pair <float,float> >();
}

std::vector < std::pair <float,float> >EffContainer::GetCicEff(){
        ComputeRatio();
        cout<<"Not implemented"<<endl;
        return std::vector < std::pair <float,float> >();
}
int EffContainer::get_entries(){
    return denominator->GetEntries();
}

void EffContainer::Write(){
    ComputeRatio();
    numerator->Write(numerator->GetTitle());
    denominator->Write(denominator->GetTitle());
    ratio->Write(ratio->GetTitle());
    teff->Write(teff->GetTitle());
}


void EffContainer::ComputeRatio(){
    int n_entries = denominator->GetEntries();
    if (n_entries != n_entries_processed){
        for (int i = 0; i < denominator->GetXaxis()->GetNbins(); i++){
            float num = numerator->GetBinContent(i+1);
            float den = denominator->GetBinContent(i+1);
            if (den == 0){
                ratio->SetBinContent(i+1,0);
                ratio->SetBinError(i+1,0);
            }
            else{
                double e = num*1./den;
                ratio->SetBinContent(i+1,e);
                ratio->SetBinError(i+1,sqrt(e*(1-e)/den));
            }
        }
        n_entries_processed = n_entries;
    }
}


HistManager::HistManager(std::string saveFile):save_file(saveFile){}

HistManager::~HistManager(){
    //TODO FIXME
    //This is a problem... Most was destroyed when closing file...
    /*for (auto h: hists)
        delete h;
    for (auto g:graphs)
        delete g;
    for (auto h2:hists_2D)
        delete h2;
    for (auto e:efficiencies)
        delete e;*/
    
}

void HistManager::Write(){
    TFile * f = new TFile(save_file.c_str(),"RECREATE");
    for (auto h: hists)
        h->Write();
    for (auto g:graphs)
        g->Write();
    for (auto h2:hists_2D)
        h2->Write();
    for (auto e:efficiencies)
        e->Write();
    for (auto e:efficiencies2D)
        e->Write();
    for (auto o: additional_objects)
        o->Write();
    f->Close();
    delete f;
}

TH1 * HistManager::AddTH1(string name, int n_bins, float x_0, float x_1){
    TH1 * h = new TH1F(name.c_str(), name.c_str(), n_bins, x_0, x_1);
    hists.push_back(h);
    return h;
}

TGraph *  HistManager::AddTGraph(string name, const vector <float> & data_x,const vector <float> & data_y){
    TGraph * g = new TGraph(data_x.size(), &(*(data_x.begin())),&(*(data_y.begin())));
    g->SetTitle(name.c_str());
    g->SetName(name.c_str());
    graphs.push_back(g);
    return g;
}

TH2 * HistManager::AddTH2(string name, int n_bins_x, float x_0, float x_1,int n_bins_y, float y_0, float y_1){
    TH2 * h = new TH2F(name.c_str(), name.c_str(), n_bins_x, x_0, x_1,n_bins_y, y_0, y_1);
    hists_2D.push_back(h);
    return h;
}

EffContainer * HistManager::AddEfficiency(string name, int n_bins, float x_0, float x_1){
    EffContainer * e = new EffContainer(name, n_bins, x_0, x_1);
    efficiencies.push_back(e);
    return e;
}
EffContainer2D * HistManager::AddEfficiency2D(string name, int n_bins_x, float x_0, float x_1,int n_bins_y, float y_0, float y_1){
    EffContainer2D * e = new EffContainer2D(name, n_bins_x, x_0, x_1,n_bins_y, y_0, y_1);
    efficiencies2D.push_back(e);
    return e;
}

TGraph * HistManager::AddTrend(std::string name, float value, float time){
    return AddTGraph(name,vector <float> (1,time),vector <float> (1,value));
    // TGraph * g = new TGraph(1,);
    // g->SetTitle(name.c_str());
    // g->SetName(name.c_str());
    // graphs.push_back(g);
    // return g;
}


void  HistManager::AddObject(TObject * obj){additional_objects.push_back(obj);}