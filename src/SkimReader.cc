#include "TreeReader.h"
#include "SkimReader.h"
#include <iostream>

using namespace std;

SkimmedEvent::SkimmedEvent(){}
SkimmedEvent::SkimmedEvent(const MultiEvent & me){ LoadMultiEvent(me);}
SkimmedEvent::~SkimmedEvent(){}

void SkimmedEvent::LoadMultiEvent(const MultiEvent & me){
    Reset();
    for (int moduleId = 0; moduleId < 4; moduleId++){
        map < uint16_t, set <uint8_t> > stub_map;

        for (auto stub : me.stub_set[moduleId])
            stub_map[stub] = me.get_bend_codes(moduleId,stub);

        stubs.push_back(stub_map);
    }
    long_bx = me.long_bx[0];
    IPbus_user_data = me.IPbus_user_data[0];

}


void SkimmedEvent::Reset(){stubs.clear();}






SkimReader::SkimReader(string fName_){
    e = new SkimmedEvent;
    _ff = new TFile(fName_.c_str());
    _tt = (TTree *) _ff->Get("");
    event_counter = 0;
    n_entries = _tt->GetEntries();
    _tt->SetBranchAddress("skimmed_event"     ,&e);
}




SkimReader::~SkimReader(){
    _ff->Close();
    delete _ff;
}

void SkimReader::peekLastEvent(){
    _tt->GetEntry(n_entries-1);
}

void SkimReader::peekFirstEvent(){
    _tt->GetEntry(0);
}

int SkimReader::getEvent(int event_number){
    if (event_number <0){
        if (event_counter < n_entries){
            _tt->GetEntry(event_counter);
            event_counter++;
            return 1;
        }
        else{
            return 0;
        }
    }
    else{
        if ((uint)event_number < n_entries){
            event_counter = event_number;
            _tt->GetEntry(event_number);
            return 1;
        }
        else{
            return 0;
        }
    }
}

uint64_t SkimReader::getEntries(){
    return n_entries;
}
string   SkimReader::getProgress(){
    return "Processed "+to_string(event_counter) + " / "+ to_string(n_entries)+" : ~" + to_string((100*event_counter)/n_entries)+"%";

}
