#include "Aligner.h"
#include "TF1.h"
#include <iostream>
#include <map>
#include <vector>
#include <algorithm>
#include "TVector.h"
using namespace std;

template<typename K, typename V>
std::pair<K,V> getMaximumValue(const std::map<K,V> &map) {
    return *std::max_element(map.begin(), map.end(), [](std::pair<K,V> const &x, std::pair<K,V> const &y) {
        return x.second < y.second;
    });
}


TF1 * get_double_pol1( TH2 * h_corr, HistManager * hm, string name){
    vector <float> x_vals;
    vector <float> y_vals;

    bool has_data_0(0);
    bool has_data_1(0);
    for (int binX = 0; binX < h_corr->GetXaxis()->GetNbins(); binX++){
        auto proj = h_corr->ProjectionY("temp",binX,binX);
        int mpv = proj->GetXaxis()->GetBinCenter(proj->GetMaximumBin());
        int max_val = proj->GetMaximum();
        if (max_val > 5 && max_val > 0.2*proj->GetEntries()){
            if (proj->GetXaxis()->GetBinCenter(binX) > 2034)
                has_data_1 = 1;
            else
                has_data_0 = 1;
            x_vals.push_back(proj->GetXaxis()->GetBinCenter(binX));
            y_vals.push_back(mpv);
        }
    }
    TF1 * aligner = new TF1(name.c_str(),"([0]+[1]*x)*(x<2034)+([2]+[3]*x)*(x>2033)",0,4066);
    aligner->SetParameters(0,1,0,1);
    int x_min = 0;
    int x_max = 4066;
    if (! has_data_0 ){    
        cout<<"Warning, no data found in CIC0, blinding it for alignment..."<<endl;
        x_min = 2034;
        aligner->SetParLimits(0,1,1);
        aligner->SetParLimits(1,1,1);
    }
    if (! has_data_1 ){    
        cout<<"Warning, no data found in CIC1 blinding it for alignment..."<<endl;
        x_max = 2034;
        aligner->SetParLimits(2,1,1);
        aligner->SetParLimits(3,1,1);
    }
    auto g = hm->AddTGraph(name,x_vals,y_vals);
    g->Fit(name.c_str(),"","",x_min,x_max);

    aligner->SetName((name+"_0").c_str());
    aligner->SetTitle((name+"_0").c_str());
    hm->AddObject(aligner);

    return aligner;
}

TF1 * r_get_double_pol1( TH2 * h_corr, HistManager * hm, string name){
    vector <float> x_vals;
    vector <float> y_vals;

    bool has_data_0(0);
    bool has_data_1(0);
    for (int binX = 0; binX < h_corr->GetYaxis()->GetNbins(); binX++){
        auto proj = h_corr->ProjectionX("temp",binX,binX);
        int mpv = proj->GetXaxis()->GetBinCenter(proj->GetMaximumBin());
        int max_val = proj->GetMaximum();
        //if (max_val > 0.1*proj->Integral() && max_val > 5){
        if (max_val > 5 && max_val > 0.2*proj->GetEntries()){
            if (proj->GetXaxis()->GetBinCenter(binX) > 2034)
                has_data_1 = 1;
            else
                has_data_0 = 1;
            x_vals.push_back(proj->GetXaxis()->GetBinCenter(binX));
            y_vals.push_back(mpv);
        }
    }
    TF1 * aligner = new TF1(name.c_str(),"([0]+[1]*x)*(x<2034)+([2]+[3]*x)*(x>2033)",0,4066);
    aligner->SetParameters(0,1,0,1);
    int x_min = 0;
    int x_max = 4066;
    if (! has_data_0 ){    
        cout<<"Warning, no data found in CIC0, blinding it for alignment..."<<endl;
        x_min = 2034;
        aligner->SetParLimits(0,1,1);
        aligner->SetParLimits(1,1,1);
    }
    if (! has_data_1 ){    
        cout<<"Warning, no data found in CIC1 blinding it for alignment..."<<endl;
        x_max = 2034;
        aligner->SetParLimits(2,1,1);
        aligner->SetParLimits(3,1,1);
    }
    auto g = hm->AddTGraph(name,x_vals,y_vals);
    g->Fit(name.c_str(),"","",x_min,x_max);

    aligner->SetName((name+"_0").c_str());
    aligner->SetTitle((name+"_0").c_str());
    hm->AddObject(aligner);

    return aligner;
}



bool   Aligner::is_valid(float x, float y){
    if (x != 0 || y !=0)
        cerr<<"X and Y parameter not yet used" <<endl;
    cerr<<"Not implemented"<<endl; 
    return true;
}

bool   Aligner::r_is_valid(float x, float y){
    if (x != 0 || y !=0)
        cerr<<"X and Y parameter not yet used" <<endl;
    cerr<<"Not implemented"<<endl; 
    return true;
}

float  Aligner::get_x(float x, float y){
    if (align_x == nullptr){
        cerr<<"Alignment not loaded!"<<endl;
        return -1;
    }
    if (y !=0)
        cerr<<"Y parameter not yet used" <<endl;
    return align_x->Eval(x);
}

float  Aligner::r_get_x(float x, float y){
    if (r_align_x == nullptr){
        cerr<<"Alignment not loaded!"<<endl;
        return -1;
    }

    if (y !=0)
        cerr<<"Y parameter not yet used" <<endl;
    return r_align_x->Eval(x);
}

float  Aligner::get_y(float x, float y){
    if (align_y == nullptr){
        cerr<<"Alignment not loaded!"<<endl;
        return -1;
    }

    if (x !=0)
        cerr<<"X parameter not yet used" <<endl;

    return align_y->Eval(y);
}

float  Aligner::r_get_y(float x, float y){
    if (r_align_y == nullptr){
        cerr<<"Alignment not loaded!"<<endl;
        return -1;
    }

    if (x !=0)
        cerr<<"X parameter not yet used" <<endl;
    return r_align_y->Eval(y);
}

Aligner::Aligner(TreeReader * tt, HistManager * hm_){
    t = tt;
    hm = hm_;
    align_x = nullptr;
    align_y = nullptr;
    r_align_x = nullptr;
    r_align_y = nullptr;
}

Aligner::Aligner(std::string input_file){
    t = nullptr;
    hm = nullptr;
    align_x = nullptr;
    align_y = nullptr;
    r_align_x = nullptr;
    r_align_y = nullptr;
    load_align(input_file);
}

Aligner::Aligner(){
    align_x = nullptr;
    align_y = nullptr;
    r_align_x = nullptr;
    r_align_y = nullptr;
    t = nullptr;
    hm = nullptr;
}

Aligner::~Aligner(){
    delete align_x;
    delete align_y;
    delete r_align_x;
    delete r_align_y;
}

vector <int> get_stubs(TreeReader * t,int module_id,int bend_0, int bend_1){
    vector <int> to_return;
    for (uint stub_id = 0; stub_id < t->e.stub.at(module_id)->size(); stub_id++){
        int s = t->e.stub.at(module_id)->at(stub_id);
        int b = t->e.bendCode.at(module_id)->at(stub_id);
        if  ((s < 2032 && b == bend_0) || (s > 2031 && b == bend_1))
            to_return.push_back(s);
    }
    return to_return;
}

bool Aligner::load_align (std::string input_file){
    TFile * if_ = new TFile(input_file.c_str());
    if (!if_)
        return false;
    align_x     = (TF1*) if_->Get("align_even_0");
    align_y     = (TF1*) if_->Get("align_odd_0");
    r_align_x   = (TF1*) if_->Get("r_align_even_0");
    r_align_y   = (TF1*) if_->Get("r_align_odd_0");

    auto tv_good_bend_codes = (TVector*) if_->Get("good_bend_codes");
    if (!(align_x && align_y && r_align_x && r_align_y)){
        return false;
    }
    for (uint ii = 0; ii < 8; ii++){
        if (good_bend_codes.size() > ii)
            good_bend_codes[ii] = (*tv_good_bend_codes)[ii];
        else
            good_bend_codes.push_back((*tv_good_bend_codes)[ii]);
    }

    if_->Close();
    delete if_;
    return true;
}

bool Aligner::store_align(std::string output_file){
    TFile * of_ = new TFile(output_file.c_str(),"RECREATE");
    if (!of_)
        return false;
    of_->cd();
    TF1 * cp_align_x   = (TF1*)align_x->Clone();
    TF1 * cp_align_y   = (TF1*)align_y->Clone();
    TF1 * cp_r_align_x = (TF1*)r_align_x->Clone();
    TF1 * cp_r_align_y = (TF1*)r_align_y->Clone();
    cp_align_x->Write();
    cp_align_y->Write();
    cp_r_align_x->Write();
    cp_r_align_y->Write();
    TVector tv_good_bend_codes(8);
    for (unsigned int ii = 0; ii < good_bend_codes.size(); ii++){
        tv_good_bend_codes[ii] = good_bend_codes.at(ii);
    }
    tv_good_bend_codes.Write("good_bend_codes");
    of_->Write();
    delete of_;
    return true;    
}


bool Aligner::load_tree(TreeReader * tt){
    if (tt != 0)
        cerr<<"Tree parameter not used yet"<<endl;
    cerr<<"Not implemented"<<endl; 
    return true;
}

bool Aligner::perform_align(){
    if (!t){
        cerr<<"TreeReader not loaded!"<<endl;
        return false;
    }
    
    // Only perform alignment on "ideal bend", so we need to find it!
    // Bwerk, that's a lot of maps... index = 2*mod_id + cic_id
    vector < map <int,int> > bends_counter(8);

    // 10k events should be plenty!
    cout<<"Getting bends"<<endl;
    for (int event_id = 0; event_id < 100000; event_id++){
        if (!(t->getEvent())){
            t->getEvent(0);
            cout<<"Warning, starting back at start of tree!"<<endl;
        }
        for (uint module_id = 0; module_id < 4; module_id++){
            for (uint stub_id = 0; stub_id < t->e.stub.at(module_id)->size(); stub_id++){
                bends_counter.at(2*module_id+(t->e.stub.at(module_id)->at(stub_id)>2032))[t->e.bendCode.at(module_id)->at(stub_id)]++;
            }
        }
    }
    
    //Getting the maximaly populated bends and filling the "good_bend_codes" vector
    for (uint map_id = 0; map_id < 8; map_id++){
        int max = -1;
        if (bends_counter.at(map_id).empty()){
            cout<<"Warning, no stubs in module "<<(int) map_id/2<<", CIC"<<map_id%2<<endl;
        }
        else{
            max = getMaximumValue(bends_counter.at(map_id)).first;
        }

        cout<<"Module "<<(int) map_id/2<<", CIC"<<map_id%2<<", bend code most seen : "<<max<<endl;
        if (good_bend_codes.size() > map_id){
            good_bend_codes.at(map_id) = max;
        }
        else{
            good_bend_codes.push_back(max);
        }

    }
    for (auto x : good_bend_codes)
        cout<<x<<endl;

    // Preparing histograms to get correlation
    auto hist_corr_even = hm->AddTH2("corr_even_modules",508,0,4064,508,0,4064); //Was 508
    auto hist_corr_odd  = hm->AddTH2("corr_odd_modules",508,0,4064,508,0,4064);

    
    // And now, let's fill these histograms... Trying with 10k stub pairs
    int n_stubs_even = 0;
    int n_stubs_odd = 0;
    int counter = 0;
    t->getEvent(0);
    while(n_stubs_even < 100000 || n_stubs_odd < 100000){
        counter++;
        if (counter%100000 == 0){
            cout<<counter << " " << n_stubs_even << " "<< n_stubs_odd<<endl;
        }
        if (!(t->getNextMultiEvent())){
            t->getEvent(0);
            cout<<"Warning, starting back at start of tree!"<<endl;
            break;
        }

        for (auto s0: get_stubs(t,0 ,good_bend_codes.at(0), good_bend_codes.at(1))){
            for (auto s2: get_stubs(t,2 ,good_bend_codes.at(4), good_bend_codes.at(5))){
                n_stubs_even++;
                hist_corr_even->Fill(s0,s2);
            }
        }
        for (auto s1: get_stubs(t,1 ,good_bend_codes.at(2), good_bend_codes.at(3))){
            for (auto s3: get_stubs(t,3 ,good_bend_codes.at(6), good_bend_codes.at(7))){
                n_stubs_odd++;
                hist_corr_odd->Fill(s1,s3);
            }
        }
    }

    // Delete previous alignments
    delete align_x;
    delete align_y;
    delete r_align_x;
    delete r_align_y;
    // Get the first set of alignment functions
    align_x = get_double_pol1(hist_corr_even,hm,"align_even");
    align_y = get_double_pol1(hist_corr_odd,hm,"align_odd");
    r_align_x = r_get_double_pol1(hist_corr_even,hm,"r_align_even");
    r_align_y = r_get_double_pol1(hist_corr_odd,hm,"r_align_odd");
    
    return true;
}

bool Aligner::has_good_bend(int module, int stub, int bend_code){
    uint bend_index = 2*module + stub > 2034;
    if (bend_index >= good_bend_codes.size()){
        cerr<<"Error, can't retrieve good bend code"<<endl;
        return 0;
    }
    return bend_code == good_bend_codes[bend_index];
}



bool Aligner::draw_residuals(bool all){
    // Do it for 100k events
    vector <TH1 *>           h_residuals;
    vector <EffContainer *>  efficiencies;
    for (int module_id = 0; module_id < 4; module_id ++){
        h_residuals.push_back(hm->AddTH1((string)"residuals_"+to_string(module_id), 201,-100.5,100.5));
        efficiencies.push_back(hm->AddEfficiency((string)"efficiency_"+to_string(module_id), 508,0,4064));
    }
    
    
    t->reset();
    int n_events = 0;
    cout<<t<<endl;
    while (t->getNextMultiEvent() && (all || n_events < 100000)){
        n_events++;
        float default_res = 100000;

        for (auto s_seed : t->me.stub_set.at(2)){
            float min_res = default_res;
            float proj_s = r_get_x(s_seed,0);
            for (auto s_match: t->me.stub_set.at(0)){
                float res = s_match-proj_s;
                if (min_res == default_res || abs(res) < abs(min_res))
                    min_res = res;
            }
            if (min_res != default_res)
                h_residuals.at(0)->Fill(min_res);
            efficiencies.at(0)->Fill(proj_s,abs(min_res) < 20);
        }

        for (auto s_seed : t->me.stub_set.at(0)){
            float min_res = default_res;
            float proj_s = get_x(s_seed,0);
            for (auto s_match: t->me.stub_set.at(2)){
                float res = s_match-proj_s;
                if (min_res == default_res || abs(res) < abs(min_res))
                    min_res = res;
            }
            if (min_res != default_res)
                h_residuals.at(2)->Fill(min_res);
            efficiencies.at(2)->Fill(proj_s,abs(min_res) < 20);

        }

        for (auto s_seed : t->me.stub_set.at(3)){
            float min_res = default_res;
            float proj_s = r_get_y(0,s_seed);
            for (auto s_match: t->me.stub_set.at(1)){
                float res = s_match-proj_s;
                if (min_res == default_res || abs(res) < abs(min_res))
                    min_res = res;
            }
            if (min_res != default_res)
                h_residuals.at(1)->Fill(min_res);
            efficiencies.at(1)->Fill(proj_s,abs(min_res) < 20);

        }        
        for (auto s_seed : t->me.stub_set.at(1)){
            float min_res = default_res;
            float proj_s = get_y(0,s_seed);
            for (auto s_match: t->me.stub_set.at(3)){
                float res = s_match-proj_s;
                if (min_res == default_res || abs(res) < abs(min_res))
                    min_res = res;
            }
            if (min_res != default_res)
                h_residuals.at(3)->Fill(min_res);
            efficiencies.at(3)->Fill(proj_s,abs(min_res) < 20);
        }
        
    }
    cout<<"DONE! Processed "<<n_events<<"events"<<endl;
    return true;
}

bool Aligner::draw_clean_residuals(bool all){
    // Do it for 100k events
    vector <TH1 *>           h_residuals;
    vector <EffContainer2D *>           efficiencies_2D;
    vector <EffContainer *>  efficiencies;
    vector <EffContainer *>  eff_fiducial;
    for (int module_id = 0; module_id < 4; module_id ++){
        h_residuals.push_back(hm->AddTH1((string)"clean_residuals_"+to_string(module_id), 201,-100.5,100.5));
        efficiencies.push_back(hm->AddEfficiency((string)"clean_efficiency_"+to_string(module_id), 508,0,4064));
        efficiencies_2D.push_back(hm->AddEfficiency2D((string)"clean_efficiency_2D_"+to_string(module_id), 508,0,4064,508,0,4064));
        eff_fiducial.push_back(hm->AddEfficiency((string)"eff_fiducial_"+to_string(module_id), 508,0,4064));
    }
    
    t->reset();
    int n_events = 0;
    while (t->getNextMultiEvent() && (all || n_events < 100000)){
        if (n_events % 100000 == 0){
            cout<<"Drawn 'clean residuals/efficiency' for "<<n_events<<" events"<<endl;
        }
        n_events++;
        float default_res = 100000;
        if (t->me.stub_set.at(1).size() == 1 && t->me.stub_set.at(2).size() == 1 && t->me.stub_set.at(3).size() == 1){
            auto b1 = *(t->me.stub_set.at(1).begin());
            auto b2 = *(t->me.stub_set.at(2).begin());
            auto b3 = *(t->me.stub_set.at(3).begin());
            // validator->Fill(b1);
            if (t->me.get_bend_codes(1,b1).count(good_bend_codes.at(2+(b1>2036))) != 0 &&
                t->me.get_bend_codes(2,b2).count(good_bend_codes.at(4+(b2>2036))) != 0 &&
                t->me.get_bend_codes(3,b3).count(good_bend_codes.at(6+(b3>2036))) != 0){
                auto s_seed = *(t->me.stub_set.at(2).begin());
                float min_res = default_res;
                float proj_s = r_get_x(s_seed,0);
                // Force hits in perpendicular direction to be in the same side, and not on the edge
                if ((b1%2036 < 1018) == (b3%2036 < 1018)){
                    for (auto s_match: t->me.stub_set.at(0)){
                        float res = s_match-proj_s;
                        if (min_res == default_res || abs(res) < abs(min_res))
                            min_res = res;
                    }
                    if(b1%1018 > 100 && b3%1018 > 100 && b1%1018 < 918 && b3%1018 < 918){
                        if (min_res != default_res)
                            h_residuals.at(0)->Fill(min_res);
                        efficiencies.at(0)->Fill(proj_s,abs(min_res) < 20);
                    }
                    if ((                 proj_s <= 1018 && b1 >  500 && b1 <  801)||
                        (proj_s > 1018 && proj_s <= 2034 && b1 > 2534 && b1 < 2834)||
                        (proj_s > 2034 && proj_s <= 3052 && b1 > 1234 && b1 < 1534)||
                        (proj_s > 3052 &&                   b1 > 3266 && b1 < 3566))
                        eff_fiducial.at(0)->Fill(proj_s,abs(min_res) < 20);

                    efficiencies_2D.at(0)->Fill(proj_s,b1,abs(min_res) < 20);
                }
            }
        }

        // Checking module #1 :
        if (t->me.stub_set.at(0).size() == 1 && t->me.stub_set.at(2).size() == 1 && t->me.stub_set.at(3).size() == 1){
            auto b0 = *(t->me.stub_set.at(0).begin());
            auto b2 = *(t->me.stub_set.at(2).begin());
            auto b3 = *(t->me.stub_set.at(3).begin());
            if (t->me.get_bend_codes(0,b0).count(good_bend_codes.at(0+(b0>2036))) != 0 &&
                t->me.get_bend_codes(2,b2).count(good_bend_codes.at(4+(b2>2036))) != 0 &&
                t->me.get_bend_codes(3,b3).count(good_bend_codes.at(6+(b3>2036))) != 0){
                auto s_seed = *(t->me.stub_set.at(3).begin());
                float min_res = default_res;
                float proj_s = r_get_y(0,s_seed);
                // Force hits in perpendicular direction to be in the same CIC, and not on the edge
                if ((b0%2036 < 1018) == (b2%2036 < 1018)){
                    for (auto s_match: t->me.stub_set.at(1)){
                            float res = s_match-proj_s;
                            if (min_res == default_res || abs(res) < abs(min_res))
                                min_res = res;
                    }

                    if (b0%1018 > 100 && b2%1018 > 100 && b0%1018 < 918 && b2%1018 < 918){
                        if (min_res != default_res)
                            h_residuals.at(1)->Fill(min_res);
                        efficiencies.at(1)->Fill(proj_s,abs(min_res) < 20);
                    }
                    if ((                 proj_s <= 1018 && b0 >  500 && b0 <  801)||
                        (proj_s > 1018 && proj_s <= 2034 && b0 > 2534 && b0 < 2834)||
                        (proj_s > 2034 && proj_s <= 3052 && b0 > 1234 && b0 < 1534)||
                        (proj_s > 3052 &&                   b0 > 3266 && b0 < 3566))
                        eff_fiducial.at(1)->Fill(proj_s,abs(min_res) < 20);
                    efficiencies_2D.at(1)->Fill(proj_s,b0,abs(min_res) < 20);
                }

            }
        }
        // Checking module #2 :
        if (t->me.stub_set.at(0).size() == 1 && t->me.stub_set.at(1).size() == 1 && t->me.stub_set.at(3).size() == 1){
            auto b0 = *(t->me.stub_set.at(0).begin());
            auto b1 = *(t->me.stub_set.at(1).begin());
            auto b3 = *(t->me.stub_set.at(3).begin());
            if (t->me.get_bend_codes(0,b0).count(good_bend_codes.at(0+(b0>2036))) != 0 &&
                t->me.get_bend_codes(1,b1).count(good_bend_codes.at(2+(b1>2036))) != 0 &&
                t->me.get_bend_codes(3,b3).count(good_bend_codes.at(6+(b3>2036))) != 0){
                auto s_seed = *(t->me.stub_set.at(0).begin());
                float min_res = default_res;
                float proj_s = get_x(s_seed,0);
                // Force hits in perpendicular direction to be in the same CIC, and not on the edge
                if ((b1%2036 < 1018) == (b3%2036 < 1018)){
                    for (auto s_match: t->me.stub_set.at(2)){
                            float res = s_match-proj_s;
                            if (min_res == default_res || abs(res) < abs(min_res))
                                min_res = res;
                    }

                    if(b1%1018 > 100 && b3%1018 > 100 && b1%1018 < 918 && b3%1018 < 918){
                        if (min_res != default_res)
                            h_residuals.at(2)->Fill(min_res);
                        efficiencies.at(2)->Fill(proj_s,abs(min_res) < 20);
                    }
                    if ((                 proj_s <= 1018 && b3 >  500 && b3 <  801)||
                        (proj_s > 1018 && proj_s <= 2034 && b3 > 2534 && b3 < 2834)||
                        (proj_s > 2034 && proj_s <= 3052 && b3 > 1234 && b3 < 1534)||
                        (proj_s > 3052 &&                   b3 > 3266 && b3 < 3566))
                        eff_fiducial.at(2)->Fill(proj_s,abs(min_res) < 20);

                    efficiencies_2D.at(2)->Fill(proj_s,b3,abs(min_res) < 20);
                }
            }
        }
        // Checking module #3 :
        if (t->me.stub_set.at(0).size() == 1 && t->me.stub_set.at(1).size() == 1 && t->me.stub_set.at(2).size() == 1){
            auto b0 = *(t->me.stub_set.at(0).begin());
            auto b1 = *(t->me.stub_set.at(1).begin());
            auto b2 = *(t->me.stub_set.at(2).begin());
            if (t->me.get_bend_codes(0,b0).count(good_bend_codes.at(0+(b0>2036))) != 0 &&
                t->me.get_bend_codes(1,b1).count(good_bend_codes.at(2+(b1>2036))) != 0 &&
                t->me.get_bend_codes(2,b2).count(good_bend_codes.at(4+(b2>2036))) != 0){
                auto s_seed = *(t->me.stub_set.at(1).begin());
                float min_res = default_res;
                float proj_s = get_y(0,s_seed);
                // Force hits in perpendicular direction to be in the same CIC, and not on the edge
                if ((b0%2036 < 1018) == (b2%2036 < 1018)){
                    for (auto s_match: t->me.stub_set.at(3)){
                        float res = s_match-proj_s;
                        if (min_res == default_res || abs(res) < abs(min_res))
                            min_res = res;
                    }
                    if (b0%1018 > 100 && b2%1018 > 100 && b0%1018 < 918 && b2%1018 < 918){
                        if (min_res != default_res)
                            h_residuals.at(3)->Fill(min_res);
                        efficiencies.at(3)->Fill(proj_s,abs(min_res) < 20);
                    }
                    if ((                 proj_s <= 1018 && b2 >  500 && b2 <  801)||
                        (proj_s > 1018 && proj_s <= 2034 && b2 > 2534 && b2 < 2834)||
                        (proj_s > 2034 && proj_s <= 3052 && b2 > 1234 && b2 < 1534)||
                        (proj_s > 3052 &&                   b2 > 3266 && b2 < 3566))
                        eff_fiducial.at(3)->Fill(proj_s,abs(min_res) < 20);

                    efficiencies_2D.at(3)->Fill(proj_s,b2,abs(min_res) < 20);
                }
            }
        }





    }

    t->peekFirstEvent();
    float time_start=t->e.long_bx/4e7;
    
    t->peekLastEvent();
    float time_stop = t->e.long_bx/4e7;
    float time=0.5*(time_start+time_stop);
    for (int i = 0; i < 4 ; i++){
        hm->AddTrend("trend_residuals_stddev_"+to_string(i),h_residuals.at(i)->GetStdDev(),time);
        hm->AddTrend("trend_residuals_avg_"+to_string(i),h_residuals.at(i)->GetMean(),time);
    }

    hm->AddTrend("trend_align_p0_m0_cic0",r_align_x->GetParameter(0),time);
    hm->AddTrend("trend_align_p0_m0_cic1",r_align_x->GetParameter(2),time);
    hm->AddTrend("trend_align_p0_m1_cic0",r_align_y->GetParameter(0),time);
    hm->AddTrend("trend_align_p0_m1_cic1",r_align_y->GetParameter(2),time);
    hm->AddTrend("trend_align_p0_m2_cic0",align_x->GetParameter(0),time);
    hm->AddTrend("trend_align_p0_m2_cic1",align_x->GetParameter(2),time);
    hm->AddTrend("trend_align_p0_m3_cic0",align_y->GetParameter(0),time);
    hm->AddTrend("trend_align_p0_m3_cic1",align_y->GetParameter(2),time);

    return true;
}




void Aligner::force_good_bend(bool status){
    if (status != 0)
        cerr<<"Status not used yet"<<endl;
    cerr<<"Not implemented"<<endl;
}
