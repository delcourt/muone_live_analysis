#include "TreeReader.h"
using namespace std;

float LUT[16] = {0, 1, 2, 3, 4, 5, 6, 7, 999 ,-1,-2,-3,-4,-5,-6,-7};


MultiEvent::MultiEvent(bool pre_process, bool keep_all):
    stub(4, vector< vector <int> >()), bend(4, vector< vector <int> >()),
    bend_code(4, vector< vector <int> >()), stub_set(4, set <int> ()),
    full_stub(4), module_status(4, vector < uint16_t > ()), 
    pre_process_(pre_process), keep_all_(keep_all),set_computed(false){
    bend_flag = false;
    bend_filter = 0;

}

void MultiEvent::set_filter_on_bend(bool flag, int bend) {
    bend_flag = flag;
    bend_filter = bend;
}


MultiEvent::~MultiEvent(){
}

set < float > MultiEvent::get_bends      (int module, int stub_) const {
    set <float> bends;
    for (uint bx = 0; bx < stub.at(module).size(); bx++){
        for (uint s_id = 0; s_id < stub.at(module).at(bx).size(); s_id++){
            if (stub_ == stub.at(module).at(bx).at(s_id))
                bends.insert(bend.at(module).at(bx).at(s_id));
        }
    }
    return bends;
}

set < uint8_t > MultiEvent::get_bend_codes (int module, int stub_) const{
    set <uint8_t> bend_codes;
    for (uint bx = 0; bx < stub.at(module).size(); bx++){
        for (uint s_id = 0; s_id < stub.at(module).at(bx).size(); s_id++){
            if (stub_ == stub.at(module).at(bx).at(s_id))
                bend_codes.insert(bend_code.at(module).at(bx).at(s_id));
        }
    }
    return bend_codes;

}


void MultiEvent::Reset(){
    for (unsigned int i = 0; i < 4; i++){
        stub[i].clear();
        bend[i].clear();
        bend_code[i].clear();
        module_status[i].clear();
        stub_set[i].clear();
        full_stub[i].clear();
    }
    long_bx.clear();
    bx.clear();
    IPbus_user_data.clear();
}

void MultiEvent::AddEvent(const event & e){    
    for (unsigned int i = 0; i < 4; i++){
        stub[i].push_back(vector <int>());
        bend[i].push_back(vector <int>());
        bend_code[i].push_back(vector <int>());

        int stub_index = -1;
        for (auto s : *(e.stub[i])){
            stub_index++;
            if (bend_flag && e.bend[i]->at(stub_index) != bend_filter)
                continue;
            stub[i].back().push_back(s);
            bend_code[i].back().push_back(e.bendCode[i]->at(stub_index));
            stub_set[i].insert(s);
        }
        for (auto b : *(e.bend[i])){
            if (bend_flag && b != bend_filter)
                continue;
            bend[i].back().push_back(b);
        }
        for (auto si: e.full_stub[i])
            full_stub[i].push_back(si);
    }
    module_status[0].push_back(e.module_status_0);
    module_status[1].push_back(e.module_status_1);
    module_status[2].push_back(e.module_status_2);
    module_status[3].push_back(e.module_status_3);
    long_bx.push_back(e.long_bx);
    bx.push_back(e.bx);
    IPbus_user_data.push_back(e.IPbus_user_data);
}


unsigned int MultiEvent::size(){
    return stub[0].size();
}



event::event(){
    for (int i = 0; i < 4; i++){
        stub.push_back(new vector <int> );
        bendCode.push_back(new vector <int>) ;
        bend.push_back(new vector <int>) ;
        full_stub.push_back(vector <stub_info>());
    }
}

event::~event(){
}


TreeReader::TreeReader(string fName_){
    _ff = new TFile(fName_.c_str());
    _tt = (TTree *) _ff->Get("flatTree");
    cout<<_tt<<endl;
    n_entries = 0;
    if (_tt == 0)  
        return;
    event_counter = 0;
    e.bx = 0;
    e.bx_super_id = 0;
    n_entries = _tt->GetEntries();
    _tt->SetBranchAddress("stub_0"     ,&(e.stub.at(0)));
    _tt->SetBranchAddress("stub_1"     ,&(e.stub.at(1)));
    _tt->SetBranchAddress("stub_2"     ,&(e.stub.at(2)));
    _tt->SetBranchAddress("stub_3"     ,&(e.stub.at(3)));
    _tt->SetBranchAddress("bend_code_0" ,&(e.bendCode.at(0)));
    _tt->SetBranchAddress("bend_code_1" ,&(e.bendCode.at(1)));
    _tt->SetBranchAddress("bend_code_2" ,&(e.bendCode.at(2)));
    _tt->SetBranchAddress("bend_code_3" ,&(e.bendCode.at(3)));

    _tt->SetBranchAddress("module_0_status" ,&(e.module_status_0));
    _tt->SetBranchAddress("module_1_status" ,&(e.module_status_1));
    _tt->SetBranchAddress("module_2_status" ,&(e.module_status_2));
    _tt->SetBranchAddress("module_3_status" ,&(e.module_status_3));

    _tt->SetBranchAddress("bx"              ,&(e.bx));
    _tt->SetBranchAddress("bx_super_id"     ,&(e.bx_super_id));
    _tt->SetBranchAddress("IPbus_user_data" ,&(e.IPbus_user_data));
    max_event_buff = 5;
    event_filled = 0;
    max_gap = 1;
}


void TreeReader::process_event(){
    for (int i = 0; i < 4; i++){
        e.bend.at(i)->clear();
        for (auto sb :*(e.bendCode.at(i))){
            e.bend.at(i)->push_back(LUT[sb]);
        }
    }
    //cout<<e.bx<<" ";
    //e.long_bx = (uint64_t) e.bx + 3564*((uint64_t)e.bx_super_id);
    e.long_bx = e.bx_super_id;
    e.long_bx *= 3564;
    e.long_bx += e.bx;
    //See CIC reference manual :
    // Note to the DAQ: if there is still 3564 BXs in the future LHC orbit, some blocks will
    //span over two LHC orbits. It is not foreseen to change anything in the CIC. In other
    //words, the block starting at BXID 3560 will still contain the data from 8BXs: the four
    //last BXs of the current orbit, and the four first BXs of the next one.

    for (int i = 0; i < 4 ; i++){
        e.full_stub.at(i).clear();
        for (uint st_id = 0; st_id < e.stub.at(i)->size(); st_id++){
            e.full_stub.at(i).push_back(stub_info());
            e.full_stub.at(i).at(st_id).add       = e.stub.at(i)->at(st_id);
            e.full_stub.at(i).at(st_id).bend      = e.bend.at(i)->at(st_id);
            e.full_stub.at(i).at(st_id).bend_code = e.bendCode.at(i)->at(st_id);
            e.full_stub.at(i).at(st_id).CIC       = e.stub.at(i)->at(st_id) > 2034;
            e.full_stub.at(i).at(st_id).long_bx   = e.long_bx;

        }

    }
}


TreeReader::~TreeReader(){
    _ff->Close();
    delete _ff;
}

int TreeReader::get_event_counter(){
    return event_counter;
}

void TreeReader::peekLastEvent(){
    if (_tt == 0)
        return;
    _tt->GetEntry(n_entries-1);
    process_event();
}

void TreeReader::peekFirstEvent(){
    if (_tt==0)
        return;
    _tt->GetEntry(0);
    process_event();
}

int TreeReader::getEvent(int event_number){
    if (_tt == 0)  
        return 0;
    event_filled = 1;
    if (event_number < 0){
        if (event_counter < n_entries){
            _tt->GetEntry(event_counter);
            process_event();
            event_counter++;
            return 1;
        }
        else{
            return 0;
        }
    }
    else{
        if ((uint) event_number < n_entries){
            event_counter = event_number;
            _tt->GetEntry(event_number);
            process_event();
            return 1;
        }
        else{
            return 0;
        }
    }
}

int TreeReader::getNextMultiEvent(){
    if (_tt == 0)
        return 0;
    if (event_filled == 0){ // Force read first tree entry
        getEvent();
    } 
    //Reset previous multi-event:
    me.Reset();
    me.AddEvent(e);
    uint64_t last_long_bx = e.long_bx;
    while (getEvent()){
        bool to_add = false;
        if (e.long_bx > last_long_bx){
            if (e.long_bx < last_long_bx + max_gap){
                to_add = true;
            }
        }
        if (e.long_bx <= last_long_bx){
            if (e.long_bx > last_long_bx - max_gap){
                to_add = true;
            }
        }

        //if (abs((uint64_t) e.long_bx - (uint64_t)last_long_bx) <= max_gap && me.size() < max_event_buff ){
        if (to_add && me.size() <  max_event_buff){
                me.AddEvent(e);
                last_long_bx = e.long_bx;
        }
        else{
            if (me.size() >= max_event_buff){
                //cout<<"Warning, buffer full"<<endl;
            }
            return 1;
        }
    } 
    return 0; 
}

void TreeReader::reset(){
    event_filled = 0;
    event_counter = 0;
    e.bx = 0;
    e.bx_super_id = 0;
}

uint64_t TreeReader::getEntries(){
    return n_entries;
}
string   TreeReader::getProgress(){
    return "Processed "+to_string(event_counter) + " / "+ to_string(n_entries)+" : ~" + to_string((100*event_counter)/n_entries)+"%";

}
