#include "Aligner.h"
#include "TreeReader.h"
#include <iostream>
using namespace std;

int main(int argc, char ** argv){
    if ( argc != 3 ){
        cerr<<"Error, expecting input and output file!"<<endl;
        return -1;
    }
    TreeReader * tt = new TreeReader(argv[1]);
    HistManager * hm = new HistManager("align_crosscheck.root");
    Aligner align(tt, hm);
    align.perform_align();
    align.store_align(argv[2]);
    
    Aligner align2(tt,hm);
    align2.load_align(argv[2]);
    align2.draw_residuals();
    align2.draw_clean_residuals();
    hm->Write();
}
