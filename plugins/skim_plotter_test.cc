#include "SkimReader.h"
#include "HistManager.h"
#include <iostream>
using namespace std;

int main(int argc, char ** argv){
    if (argc != 3){
        cout<<"Error, expecting 2 args..."<<endl;
        return 1;
    }
    cout<<argv[0]<<" "<<argv[1]<<" "<<argv[2]<<endl;

    SkimReader  sr(argv[1]);
    HistManager hm(argv[2]);
    auto bp = hm.AddTH1("beam_profile",4096,0,4096);
    int event_counter = 0;
    while (sr.getEvent()){
        event_counter++;
        // cout<<event_counter<<endl;
        for (auto s : sr.e->stubs[0])
            bp->Fill(s.first);
        if (event_counter % 100000 == 0)
            cout<<sr.getProgress()<<endl;
    }

}