#include "TreeReader.h"
#include <iostream>
#include <dirent.h>
using namespace std;

vector <string> get_file_list(string path){
    vector <string> f_list;
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir (path.c_str())) != NULL) {
        /* print all the files and directories within directory */
        while ((ent = readdir (dir)) != NULL) {
            f_list.push_back(ent->d_name);
        }
        closedir (dir);
    }
    return f_list;

}

pair <int,int> get_index(string file_name){
    pair <int,int> index;
    index.first = -1;
    index.second = -1;

    if (count(file_name.begin(), file_name.end(), '.') == 3)
    {
        int first_dot = file_name.find(".");
        int second_dot = file_name.find(".",first_dot+1);
        int third_dot = file_name.find(".",first_dot+1);
        index.first=atoi(file_name.substr(first_dot+1,second_dot-first_dot-1).c_str());
        index.second=atoi(file_name.substr(second_dot+1,third_dot-second_dot-1).c_str());
    }

    return index;
}

vector <string> get_file_list_filtered(string path, int first_index, int second_index){
    vector <string> f_list;
    auto all_files = get_file_list(path);
    for (auto ff : all_files){
        auto index = get_index(ff);
        if (first_index == -1 || index.first == first_index){
            if (second_index == -1 || index.second == second_index){
                f_list.push_back(ff);
            }
        }
    }
    return f_list;
}

int main() {

    string path = "/eos/experiment/mu-e/daq/2022/bel-ntuple/run_3049/";



    set <int> first_index;
    for (auto f:get_file_list(path)){
        first_index.insert(get_index(f).first);
    }
    first_index.erase(-1);

    
    for (auto ii: first_index){
        set <int> second_index;
        second_index.erase(-1);

        for (auto f : get_file_list_filtered(path,ii,-1))
            second_index.insert(get_index(f).second);
        for (auto jj: second_index){
            if (jj > 1)
                continue;
            if(get_file_list_filtered(path,ii,jj).size()!= 1){
                cout<<"ERROR, MORE THAN ONE FILE"<<endl;
                break;
            }
            string file = get_file_list_filtered(path,ii,jj).at(0);


            cout<<"Opening file "<<file<<" (index : "<<ii<<" "<<jj<<")"<<endl;
            TreeReader tr(path+file);
            
            if (tr.getEntries() == 0){
                cout<<"Empty tree... Exiting..."<<endl;
                break;
            }
            
            //Check if all entries are banned:
            tr.peekFirstEvent();
            uint32_t start_IP = tr.e.IPbus_user_data;
            tr.peekLastEvent();
            uint32_t stop_IP = tr.e.IPbus_user_data;
            cout<<file<<" : "<<start_IP<<" "<<stop_IP<<endl;
        }
    }
}


