#include "TreeReader.h"
#include "HistManager.h"
#include "Aligner.h"
#include <iostream>
#include <map>
#include <iostream>

using namespace std;


int main(int argc, char ** argv){
    if (argc != 4){
        cerr<<"Expecting 3 args : Input file, Output file, Alignment file"<<endl;
        return 1;
    }

    auto tr = TreeReader(argv[1]);
    auto hm = HistManager(argv[2]);
    auto al = Aligner(argv[3]);

    map <int, long int> n_events;
    vector < map <int, long int> > n_stubs(4);
    map <uint64_t, map <int, long int> > IP_bus;
    long int evt_counter = 0;

    vector < map <int, map <int, pair <int,int>> >> parity_container(8);

    map <int, vector <int> > alignment_container_x;
    map <int, vector <int> > alignment_container_y;

    //Fill in maps


    while (tr.getEvent()){
        evt_counter++;
        // cout<<evt_counter<<endl;
        if (tr.e.IPbus_user_data > 1000 || tr.e.IPbus_user_data == 0)
            continue;
        int time = tr.e.long_bx / 4e4;
        if (evt_counter%1000000 == 0)
            cout<<tr.getProgress()<<endl;
        // Remove any time > 24h... this is not physical...
        if (time > 8640000)
            continue;
        n_events[time] ++;
        IP_bus[tr.e.IPbus_user_data][time]++;
        for (int modId = 0; modId < 4; modId++)
            n_stubs[modId][time] += tr.e.stub.at(modId)->size();

        //Checking parity
        for (uint modId = 0; modId < 4; modId++){
            for (uint stubId = 0; stubId < tr.e.stub.at(modId)->size(); stubId++){
                int stub = tr.e.stub.at(modId)->at(stubId);
                int cic_index = 2*modId + (stub > 2034);
                parity_container[cic_index][tr.e.bendCode.at(modId)->at(stubId)][time].first++;
                parity_container[cic_index][tr.e.bendCode.at(modId)->at(stubId)][time].second+=tr.e.stub.at(modId)->at(stubId)%2;
            }
        }

        //Checking alignment
        // First, make sure the aligner vector exists:
        if (alignment_container_x[time].size() == 0){
            alignment_container_x[time] = vector < int > (41);
            alignment_container_y[time] = vector < int > (41);
        }
        
        //Alignment in x:
        for (auto stub: *(tr.e.stub.at(0))){
            float residuals = 999;
            float seed = al.get_x(stub,0);
            for (auto match : *(tr.e.stub.at(2))){
                if (abs(seed-match) < abs(residuals)){
                    residuals = seed-match;
                }
            }
            if (abs(residuals) < 20.5){
                int bin = 20.5 + residuals;
                alignment_container_x[time].at(bin)++;
            }
        }

        for (auto stub: *(tr.e.stub.at(1))){
            float residuals = 999;
            float seed = al.get_y(0,stub);
            for (auto match : *(tr.e.stub.at(3))){
                if (abs(seed-match) < abs(residuals)){
                    residuals = seed-match;
                }
            }
            if (abs(residuals) < 20.5){
                int bin = 20.5 + residuals;
                alignment_container_y[time].at(bin)++;
            }
        }

        
        

    }


    //Create Graphs
    
    vector <float> time;
    vector <float> rate;
    for (auto entry:n_events){
        time.push_back(entry.first);
        rate.push_back(entry.second);
    }

    for (int modId = 0; modId < 4; modId++){
        vector <float> stub_rate;
        for (auto entry:n_stubs[modId])
            stub_rate.push_back(entry.second);
        hm.AddTGraph((string)"stub_rate_"+to_string(modId),time,stub_rate);
    }
    hm.AddTGraph("event_rate",time,rate);
    



    for (auto IP_data:IP_bus){
        auto IP_val   = IP_data.first;
        auto time_map = IP_data.second;
        vector <float> IP_time;
        vector <float> IP_rate;
        for (auto entry:time_map){
            IP_time.push_back(entry.first);
            IP_rate.push_back(entry.second);
        }
        hm.AddTGraph((string) "IP_rate_"+to_string(IP_val), IP_time,IP_rate);
    }

    for (int modId = 0; modId < 8; modId++){
        for (auto bendTup: parity_container.at(modId)){
            int bend = bendTup.first;
            vector <float> t_v;
            vector <float> parity_r;
            vector <float> parity_num;
            vector <float> parity_den;
            for (auto tup: bendTup.second){
                t_v.push_back(tup.first);
                parity_r.push_back(tup.second.second*1./tup.second.first);
                parity_den.push_back(tup.second.first);
                parity_num.push_back(tup.second.second);
            }
            hm.AddTGraph("parity_time_ratio_"+to_string(modId)+"_"+to_string(bend),t_v,parity_r);
            hm.AddTGraph("parity_time_num_"+to_string(modId)+"_"+to_string(bend),t_v,parity_num);
            hm.AddTGraph("parity_time_den_"+to_string(modId)+"_"+to_string(bend),t_v,parity_den);

        }
    }

    vector <float> tv_al;
    vector <float> avgx_v;
    vector <float> rmsx_v;
    vector <float> nx_v;
    vector <float> ny_v;
    vector <float> avgy_v;
    vector <float> rmsy_v;
    

    //Adding alignment graphs:
    for (auto tup_align: alignment_container_x){
        int time = tup_align.first;
        vector <int> x_vec = tup_align.second;
        vector <int> y_vec = alignment_container_y[time];
        float avg_x = 0;
        float avg_y = 0;
        float rms_x = 0;
        float rms_y = 0;
        int   n_x   = 0;
        int   n_y   = 0;

        for (int ii = 0; ii < 41; ii++){
            avg_x += x_vec.at(ii)*(ii-20);
            n_x   += x_vec.at(ii);
            avg_y += y_vec.at(ii)*(ii-20);
            n_y   += y_vec.at(ii);
        }
        // cout<<n_x<<" "<<n_y<<endl;
        if (n_x == 0)
            n_x = 1;
        if (n_y == 0)
            n_y = 1;

        avg_x *= 1./n_x;
        avg_y *= 1./n_y;
        for (int ii = 0; ii < 41; ii++){
            rms_x += x_vec.at(ii)*pow((ii-20-avg_x),2);
            rms_y += y_vec.at(ii)*pow((ii-20-avg_y),2);
        }
        rms_x *= 1./n_x;
        rms_y *= 1./n_y;
        rms_x = pow(rms_x,0.5);
        rms_y = pow(rms_y,0.5);
        
        tv_al.push_back(time);
        rmsx_v.push_back(rms_x);
        rmsy_v.push_back(rms_y);
        avgx_v.push_back(avg_x);
        avgy_v.push_back(avg_y);
        nx_v.push_back(n_x);
        ny_v.push_back(n_y);
    }
    hm.AddTGraph("al_avg_x",tv_al,avgx_v);
    hm.AddTGraph("al_avg_y",tv_al,avgy_v);
    hm.AddTGraph("al_rms_x",tv_al,rmsx_v);
    hm.AddTGraph("al_rms_y",tv_al,rmsy_v);
    hm.AddTGraph("al_nx",tv_al,nx_v);
    hm.AddTGraph("al_ny",tv_al,ny_v);
    

    hm.Write();
    return 0;
}


