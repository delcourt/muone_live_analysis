#include "TreeReader.h"
#include "SkimReader.h"
#include <iostream>
using namespace std;

int main(int argc, char ** argv){
    if (argc != 3){
        cout<<"Error, expecting 2 args..."<<endl;
        return 1;
    }
    cout<<argv[0]<<" "<<argv[1]<<" "<<argv[2]<<endl;
    TreeReader  tr(argv[1]);

    TFile * output = new TFile(argv[2],"RECREATE");
    TTree * t2 = new TTree();
    SkimmedEvent se;

    t2->Branch("skimmed_event"     ,&se);
    t2->SetDirectory(output);
    int eventId = 0;
    while(tr.getNextMultiEvent()){
        eventId++;
        se.LoadMultiEvent(tr.me);
        t2->Fill();
        if (eventId % 100000 == 0)
            cout<<eventId<<endl;
    }

}