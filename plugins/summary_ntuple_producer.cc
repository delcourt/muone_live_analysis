#include "TreeReader.h"
#include <iostream>
#include <dirent.h>
using namespace std;

vector <string> get_file_list(string path){
    vector <string> f_list;
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir (path.c_str())) != NULL) {
        /* print all the files and directories within directory */
        while ((ent = readdir (dir)) != NULL) {
            f_list.push_back(ent->d_name);
        }
        closedir (dir);
    }
    return f_list;

}

pair <int,int> get_index(string file_name){
    pair <int,int> index;
    index.first = -1;
    index.second = -1;

    if (count(file_name.begin(), file_name.end(), '.') == 3)
    {
        int first_dot = file_name.find(".");
        int second_dot = file_name.find(".",first_dot+1);
        int third_dot = file_name.find(".",first_dot+1);
        index.first=atoi(file_name.substr(first_dot+1,second_dot-first_dot-1).c_str());
        index.second=atoi(file_name.substr(second_dot+1,third_dot-second_dot-1).c_str());
    }

    return index;
}

vector <string> get_file_list_filtered(string path, int first_index, int second_index){
    vector <string> f_list;
    auto all_files = get_file_list(path);
    for (auto ff : all_files){
        auto index = get_index(ff);
        if (first_index == -1 || index.first == first_index){
            if (second_index == -1 || index.second == second_index){
                f_list.push_back(ff);
            }
        }
    }
    return f_list;
}

int main() {
    string path = "/eos/experiment/mu-e/daq/2022/bel-ntuple/run_3022/";
    string output_file = "test.root";


    TFile * of = new TFile(output_file.c_str(), "RECREATE");
    TTree * t2 = new TTree();

    vector < vector <int> *>       stub(4);
    vector < vector <int> *>       bendCode(4);
    vector < vector <int> *>       bend(4);
    uint16_t module_status_0;
    uint16_t module_status_1;
    uint16_t module_status_2;
    uint16_t module_status_3;
    uint16_t bx;
    uint32_t bx_super_id;
    uint32_t IPbus_user_data;

    t2->Branch("stub_0"     ,&stub.at(0));
    t2->Branch("stub_1"     ,&stub.at(1));
    t2->Branch("stub_2"     ,&stub.at(2));
    t2->Branch("stub_3"     ,&stub.at(3));
    t2->Branch("bend_code_0",&bendCode.at(0));
    t2->Branch("bend_code_1",&bendCode.at(1));
    t2->Branch("bend_code_2",&bendCode.at(2));
    t2->Branch("bend_code_3",&bendCode.at(3));
    t2->Branch("bx_super_id",&bx_super_id);
    t2->Branch("IPbus_user_data",&IPbus_user_data);

    t2->Branch("module_0_status", &module_status_0);
    t2->Branch("module_1_status", &module_status_1);
    t2->Branch("module_2_status", &module_status_2);
    t2->Branch("module_3_status", &module_status_3);
    t2->Branch("bx"       ,&bx);

    t2->SetDirectory(of);



    int max_events = 0; //(int) 1e6;

    set <int> first_index;
    for (auto f:get_file_list(path)){
        first_index.insert(get_index(f).first);
    }
    first_index.erase(-1);

    // Data selection tables:
    set <uint32_t>           banned_values;
    map <uint32_t,uint64_t>  start_time;
    map <uint32_t, uint64_t> stored_events;
    float ramp_time  = 0; //30*4e7; //in bx
    float skip_val  = 1e7;
    float skip_mult = 1.44;
    int skip_max  = 1e7; 

    for (auto ii: first_index){
        set <int> second_index;
        second_index.erase(-1);
        if (ii != 19)
            continue;
        for (auto f : get_file_list_filtered(path,ii,-1))
            second_index.insert(get_index(f).second);
        for (auto jj: second_index){
            if(get_file_list_filtered(path,ii,jj).size()!= 1){
                cout<<"ERROR, MORE THAN ONE FILE"<<endl;
                break;
            }
            string file = get_file_list_filtered(path,ii,jj).at(0);


            cout<<"Opening file "<<file<<" (index : "<<ii<<" "<<jj<<")"<<endl;
            TreeReader tr(path+file);
            
            if (tr.getEntries() == 0){
                cout<<"Empty tree... Exiting..."<<endl;
                break;
            }
            
            //Check if all entries are banned:
            tr.peekFirstEvent();
            uint32_t start_IP = tr.e.IPbus_user_data;
            uint64_t start_bx = tr.e.long_bx;
            tr.peekLastEvent();
            uint32_t stop_IP = tr.e.IPbus_user_data;
            uint64_t stop_bx = tr.e.long_bx;
            if (start_bx > 1106 * 4e7)
                {cout<<"Too late..."<<endl; continue;}
            if (stop_bx < 1100 * 4e7)
                {cout<<"Too early..."<<endl; continue;}
            if (banned_values.find(start_IP) != banned_values.end() &&
                banned_values.find(stop_IP)  != banned_values.end()){
                cout<<"Both start and stop IP in banned keys... Continuing..."<<endl;
                continue;
            }
            
            if (tr.e.long_bx > start_time[stop_IP] && (tr.e.long_bx - start_time[stop_IP]) < ramp_time){
                cout<<"Last entry is before the end of ramp..."<<endl;
                continue;
            }
        
            tr.reset(); 
            while (tr.getEvent()){
                uint32_t IP = tr.e.IPbus_user_data;

                if (IP > 2000 || IP == 0) // This means the event is corrupted...
                    continue;



                // Check if IP_value is banned            
                if (banned_values.find(IP) != banned_values.end()){
                    cout<<"Bias of "<<IP<<"V banned. Skipping "<<skip_val<<" events. "<<tr.getProgress()<<endl;            
                    bool rc = tr.getEvent(tr.get_event_counter()+skip_val);
                    skip_val *= skip_mult;
                    if (skip_val > skip_max){
                        skip_val = skip_max;
                    }
                    if (rc == 0){
                        cout<<"EOF reached"<<endl;
                        break;
                    }
                }

                
                if (start_time[IP] == 0)
                    start_time[IP] = tr.e.long_bx;
                
                if ( (tr.e.long_bx < 1100*4e7)){

                //(tr.e.long_bx < start_time[IP] || (tr.e.long_bx - start_time[IP]) < ramp_time){
                    // cout<<"Bias of "<<IP<<" not reached."<<tr.getProgress()<<" t0 = "<<start_time[IP]/4e7<<", now = "<<tr.e.long_bx/4e7<<endl;            
                    cout<<"Start time not reached "<<tr.getProgress()<<endl;
                    bool rc = tr.getEvent(tr.get_event_counter()+skip_val);
                    skip_val *= skip_mult;
                    if (skip_val > skip_max){
                        skip_val = skip_max;
                    }
                    if (rc == 0){
                        cout<<"EOF reached"<<endl;
                        break;
                    }

                    continue;
                }
                if (tr.e.long_bx > 1106*4e7){
                    break;
                }

                stub = tr.e.stub;
                bendCode=tr.e.bendCode;
                bend = tr.e.bend;
                module_status_0 = tr.e.module_status_0;
                module_status_1 = tr.e.module_status_1;
                module_status_2 = tr.e.module_status_2;
                module_status_3 = tr.e.module_status_3;
                bx = tr.e.bx;
                bx_super_id = tr.e.bx_super_id;
                IPbus_user_data = tr.e.IPbus_user_data;
                t2->Fill();
                stored_events[IP]++;

                if (max_events && stored_events[IP] >= (uint) max_events){
                    banned_values.insert(IP);
                    if (stop_IP == IP){
                        cout<<"All remaining events are from banned value"<<endl;
                        break;
                    }
                }

            }
            
        }
    }
    of->cd();
    t2->Write("flatTree");
    of->Write();

}
