#include "TreeReader.h"
#include "HistManager.h"
#include <iostream>
using namespace std;


int main(int argc, char ** argv){
    if (argc != 3){
        cout<<"Error, expecting 2 args..."<<endl;
        return 1;
    }

    TreeReader  tr(argv[1]);
    uint64_t evt_counter = 0;

    while(tr.getNextMultiEvent()){
        evt_counter++;
        cout<<"New multi-event : ";
        for (auto bx:tr.me.bx){
            cout<<bx<< " ";
        }
        cout<<endl;
        if (evt_counter > 1000)
            break;
    }

    return 0;
}