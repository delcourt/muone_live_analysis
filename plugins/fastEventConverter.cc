#include "TreeReader.h"
#include "FastReader.h"
#include "SkimReader.h"
#include <iostream>
using namespace std;

int main(int argc, char ** argv){
    if (argc != 3){
        cout<<"Error, expecting 2 args..."<<endl;
        return 1;
    }
    cout<<argv[0]<<" "<<argv[1]<<" "<<argv[2]<<endl;
    TreeReader  tr(argv[1]);

    // TFile * output = new TFile(argv[2],"RECREATE","",0);
    TFile * output = new TFile(argv[2],"RECREATE");
    TTree * t2 = new TTree("fast_event","fast_event");
    FastMultiEvent fe;
    for (int modId = 0; modId < 4; modId++)
        fe.full_stub.push_back(vector<small_stub>());
    t2->Branch("fast_event"     ,&fe);
    t2->SetAutoSave(0);

    int eventId = 0;
    
    while(tr.getNextMultiEvent()){
        eventId++;
        fe.long_bx = tr.me.long_bx.at(0);
        for (int modId = 0; modId < 4; modId++){
            fe.full_stub.at(modId).clear();
            for (auto st:tr.me.full_stub.at(modId)){
                small_stub ss;
                ss.add  = st.add;
                ss.CIC  = (st.add > 2032+2);
                ss.bend = st.bend;
                ss.bx_offset = st.long_bx-fe.long_bx;
                fe.full_stub.at(modId).push_back(ss);
            }
        }
        t2->Fill();
        if (eventId % 1000000 == 0){
            cout<<eventId<<endl;
            if (eventId == 5000000)
                break;
        }
    }
    t2->Write();
    cout<<"Total number of events : "<<eventId<<endl;
    output->Close();
}