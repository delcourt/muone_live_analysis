 #include "TreeReader.h"
 #include "TH1I.h"
 #include "TFile.h"
 #include "TH2I.h"

#include <map>
#include <iostream>
using namespace std;


uint16_t cic_mapper[8] = {0,1,2,3,7,6,5,4};

inline uint16_t cbc_shuffler( int stubAdd, int cbcId, int cicId){
    if (cicId == 0)
        return stubAdd + 254*cic_mapper[cbcId];
    else
        return 4068 - stubAdd - 254*cic_mapper[cbcId];
}



int main(int argc, char ** argv){
    if (argc != 2){
        std::cerr<<"Expecting exactly one argument"<<std::endl;
        return 1;
    }

    string input = argv[1];
    std::cout<<"Input file : "<<input<<std::endl;    

    TreeReader  tr(input);
    
    int n_stubs_processed = 0;
    int n_events_processed = 0;
    bool dump = false;
    while(!dump && tr.getNextMultiEvent()){
        n_events_processed++;
        if (n_events_processed%100000 == 0){
            cout<<n_events_processed<<" multi-events, with "<<n_stubs_processed<<" stubs processed."<<endl;
        }

        //Single module stuff
        for (uint modId = 0; modId < 4; modId++){
            //Stub persistance
            
            for (uint bx = 0; bx < tr.me.stub[modId].size(); bx++){
                map <int,int> cic_occ;
                map <int,int> cbc_occ;
                if (tr.me.stub[modId].at(bx).size() > 0){
                    //Split by CIC/ CBC(?)
                    for (auto stub: tr.me.stub[modId].at(bx)){
                        int cic = 0;
                        int cbc = 0;
                        if (stub < 2){
                            cout<<"HEY???"<<endl;
                            dump = true;
                            cout<<"BX = "<<tr.me.bx.at(bx)<<endl;
                        }
                        if (stub < 2035){
                            cbc = (stub-2)/254;
                        }
                        else{
                            cic = 1;
                            cbc = 8+(4068-stub-2)/254;
                        }

                        cic_occ[cic]++;
                        cbc_occ[cbc]++;
                    }

                    for (auto occ : cbc_occ){
                        if (occ.second > 3){
                            cout<<tr.e.bx_super_id<<endl;
                            cout<<"ERROR, too many stubs on Module "<<modId<<", CBC"<<occ.first<<endl;
                            dump = true;
                        }
                    }
                }
            }
        }
    }
    cout<<"Done"<<endl;
    if (dump){
        cout<<"EVENT DUMP :"<<endl;
        cout<<tr.e.bx_super_id<<endl;
        cout<<tr.e.bx<<endl;
        for (uint modId = 0; modId < 4; modId++){
            cout<<"MODULE #"<<modId<<endl;
            for (uint bx = 0; bx < tr.me.stub[modId].size(); bx++){
                cout<<"BX : "<<tr.me.bx[bx]<<endl;
                for (auto stub: tr.me.stub[modId].at(bx)){
                    int cbc = 0;
                    int add = 0;
                    int cic = 0;
                    if (stub < 2035){
                        cbc = (stub-2)/254;
                        add = stub-254*cbc;
                    }
                    else{
                        cic = 1;
                        cbc = (4068-stub-2)/254;
                        //stub = 4068 - stubAdd - 254*cic_mapper[cbcId];
                        add = 4068-stub-254*cbc;
                    }
                    cout<<stub << "--->"<<cic<<" "<<cbc<<" "<<add<<endl;
                }
            }
        }
    }
    uint bx_to_check          = 1024;//tr.e.bx;
    uint bx_super_id_to_check = tr.e.bx_super_id;

    tr.reset();
    while(tr.getEvent()){
        n_events_processed++;
        //Single module stuff
        if (tr.e.bx_super_id == bx_super_id_to_check && tr.e.bx > bx_to_check-8 && tr.e.bx < bx_to_check+8){
            for (int modId = 0; modId < 4; modId ++){
                for (auto stub: *(tr.e.stub[modId])){
                    int cbc = 0;
                    int add = 0;
                    if (stub < 2035){
                        cbc = (stub-2)/254;
                        add = stub-254*cbc;
                    }
                    else{
                        cbc = (4068-stub-2)/254;
                        //stub = 4068 - stubAdd - 254*cic_mapper[cbcId];
                        add = 4068-stub-254*cbc;
                    }
                    cout<<"BX"<<tr.e.bx<<", Module#"<<modId<<" : CBC "<<cic_mapper[cbc]<<" Add "<<add<<endl;
                }
            }
        }
    }
 }
 


