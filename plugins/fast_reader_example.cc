#include "FastReader.h"
#include "HistManager.h"
#include <iostream>
using namespace std;

int main(int argc, char ** argv){
    if (argc != 3){
        cerr<<"Error, expecting exactly two arguments (input and output files)"<<endl;
    }

    FastReader tr(argv[1]);
    HistManager hm(argv[2]);

    auto hist = hm.AddTH1("Beam_profile_1",4096,0,4096);
    int n_events = 0;
    cout<<tr.getEntries()<<endl;
    while (tr.getEvent()){
        for (auto ss : tr.me.full_stub.at(1))
            hist->Fill(ss.add);
        n_events++;
    }
    cout<<"N events: "<<n_events<<endl;
    hm.Write();
}
