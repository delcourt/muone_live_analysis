#include "TreeReader.h"
#include "HistManager.h"
#include "Aligner.h"
#include <iostream>
using namespace std;


vector <TH1*> multi_TH1(HistManager & hm, string name, int nbins, int x0, int x1, int n_hists = 4){
    vector <TH1 *> m_hist;
    for (int i = 0; i < n_hists; i++){
        m_hist.push_back(hm.AddTH1((name+"_"+to_string(i)).c_str(), nbins, x0, x1));
    }
    return m_hist;
}

int main(){
    string input_file = "bias_scan_1100-1106.root";
    string output_file = "hist_bias_1100.root";

    auto hm = HistManager(output_file);
    auto tr = TreeReader(input_file);
    long int align_pos = 59002627;
    tr.getEvent(align_pos);
    //Getting alignment...
    bool found_alignment = false;
    while (found_alignment == false && tr.getNextMultiEvent()){
        cout<<tr.me.IPbus_user_data.at(0)<<endl;
        if (tr.me.IPbus_user_data.at(0) > 255){
            // DO ALIGNMENT
            found_alignment = true;
        }
        if (!tr.getEvent(tr.get_event_counter()+1e5)){
            cout<<"EOF reached"<<endl;
            break;
        }

    }
    if (!found_alignment){
        cout<<"Couldn't find any valid alignment data..."<<endl;
        return 0;
    }
    cout<<"Found alignment after "<<tr.get_event_counter()<<"events "<<endl;
    Aligner align(&tr, &hm);
    align.perform_align();
    cout<<"GOOD BENDS : "<<align.good_bend_codes.at(2)<<" "<<align.good_bend_codes.at(3)<<endl;
    // for (auto x: align.good_bend_codes)
    //     cout<<x<<endl;
    //Let's gooooooo!
    tr.reset();
        // map <uint32_t, EffContainer *> efficiencies;    
    map <uint32_t, TH1* > n_stubs;

    int loop_counter = 0;
    map <uint32_t, int> IP_counter;
    vector <map <uint32_t, int>> stub_counter(4);
    map <uint32_t, TH1 *> h_residuals;
    map <uint32_t, EffContainer *> efficiencies;
    map <uint32_t, EffContainer2D *> efficiencies_2D;
    map <uint32_t, EffContainer *> eff_fiducial;
    map <uint32_t, TH1 *>          h_cluster_parity;
    map <uint32_t, TH1 *>          h_cluster_parity_bend_select;

    map <int, pair <int,int> > parity_container;

    while(tr.getEvent()){
        if (loop_counter %100000 == 0)
            cout<<tr.getProgress()<<endl;
        loop_counter++;
        uint32_t IP = tr.e.IPbus_user_data;
        if (n_stubs.count(IP) == 0){
            // Initialize plots :
            n_stubs[IP] = hm.AddTH1("n_stubs_V="+to_string(IP),50,0,50);

        h_residuals[IP]     = (hm.AddTH1         ("clean_residuals_"+to_string(IP)    , 201,-100.5,100.5));
        efficiencies[IP]    = (hm.AddEfficiency  ("clean_efficiency_"+to_string(IP)   , 508,0,4064));
        efficiencies_2D[IP] = (hm.AddEfficiency2D("clean_efficiency_2D_"+to_string(IP), 508,0,4064,508,0,4064));
        eff_fiducial[IP]    = (hm.AddEfficiency  ("eff_fiducial_"+to_string(IP)       , 508,0,4064));

        h_cluster_parity[IP] =(hm.AddTH1         ("cluster_parity_"+to_string(IP)    , 2,-0.5,1.5));
        h_cluster_parity_bend_select[IP] =(hm.AddTH1         ("cluster_parity_bend_select_"+to_string(IP)    , 2,-0.5,1.5));


        }
        if (tr.e.stub.at(0)->size() > 0 && tr.e.stub.at(2)->size() > 0 && tr.e.stub.at(3)->size() > 0){
            n_stubs[IP]->Fill(tr.e.stub.at(1)->size());
        }


        for (uint stubId = 0; stubId < tr.e.stub.at(1)->size(); stubId++){
            if (tr.e.bendCode.at(1)->at(stubId) == align.good_bend_codes.at(2+(tr.e.stub.at(1)->at(stubId)>2036))){
                h_cluster_parity_bend_select[IP]->Fill(tr.e.stub.at(1)->at(stubId)%2);
                parity_container[(int)(tr.e.long_bx/4e6)].first++;
                parity_container[(int)(tr.e.long_bx/4e6)].second+=tr.e.stub.at(1)->at(stubId)%2;
            }       
        }

        for (int i = 0; i < 4; i++)
            stub_counter[i][IP]+=tr.e.stub.at(i)->size();
        IP_counter[IP]++;

        float default_res = 100000;
        // Checking module #1 :
        if (tr.e.stub.at(0)->size() == 1 && tr.e.stub.at(2)->size() == 1 && tr.e.stub.at(3)->size() == 1){
            int b0 = tr.e.bendCode.at(0)->at(0);
            int b2 = tr.e.bendCode.at(2)->at(0);
            int b3 = tr.e.bendCode.at(3)->at(0);
            
            int s0 = tr.e.stub.at(0)->at(0);
            int s2 = tr.e.stub.at(2)->at(0);
            int s3 = tr.e.stub.at(3)->at(0);

            if (b0 == align.good_bend_codes.at(0+(s0>2036)) &&
                b2 == align.good_bend_codes.at(4+(s2>2036)) &&
                b3 == align.good_bend_codes.at(6+(s3>2036))){
                auto s_seed = s3;
                float min_res = default_res;
                int  closest_match = 0;
                float proj_s = align.r_get_y(0,s_seed);
                // Force hits in perpendicular direction to be in the same CIC, and not on the edge
                if ((s0%2036 < 1018) == (s2%2036 < 1018)){
                    for (auto s_match: *(tr.e.stub.at(1))){
                            float res = s_match-proj_s;
                            if (min_res == default_res || abs(res) < abs(min_res)){
                                min_res = res;
                                closest_match = s_match;
                            }
                    }
                    if (s0%1018 > 100 && s2%1018 > 100 && s0%1018 < 918 && s2%1018 < 918){
                        if (min_res != default_res)
                            h_residuals[IP]->Fill(min_res);
                        efficiencies[IP]->Fill(proj_s,abs(min_res) < 18);
                    }
                    if ((                 proj_s <= 1018 && s0 >  500 && s0 <  801)||
                        (proj_s > 1018 && proj_s <= 2034 && s0 > 2534 && s0 < 2834)||
                        (proj_s > 2034 && proj_s <= 3052 && s0 > 1234 && s0 < 1534)||
                        (proj_s > 3052 &&                   s0 > 3266 && s0 < 3566)){
                        eff_fiducial[IP]->Fill(proj_s,abs(min_res) < 18);
                        if (abs(min_res) < 18)
                            h_cluster_parity[IP]->Fill(closest_match%2);
                        }
                    efficiencies_2D[IP]->Fill(proj_s,s0,abs(min_res) < 18);
                }

            }
        }
    }











    for (int modId = 0; modId < 4; modId++){
        vector <float> x;
        vector <float> y;
        for (auto val: stub_counter[modId]){
            x.push_back(val.first);
            y.push_back(val.second);
        }
            
        hm.AddTGraph("N_stubs_"+to_string(modId),x,y);
    }
    vector <float> x;
    vector <float> y;
    for (auto val: IP_counter){
        x.push_back(val.first);
        y.push_back(val.second);
    }
            
    hm.AddTGraph("IP_counter",x,y);

    vector <float> t_v;
    vector <float> parity_r;
    vector <float> parity_num;
    vector <float> parity_den;

    for (auto tup: parity_container){
        t_v.push_back(tup.first*0.1);
        parity_r.push_back(tup.second.second*1./tup.second.first);
        parity_den.push_back(tup.second.first);
        parity_num.push_back(tup.second.second);

    }
    hm.AddTGraph("parity_time_ratio",t_v,parity_r);
    hm.AddTGraph("parity_time_num",t_v,parity_num);
    hm.AddTGraph("parity_time_den",t_v,parity_den);


    hm.Write();
    return 0;
}