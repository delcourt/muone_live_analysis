#include "TreeReader.h"
#include "Aligner.h"
#include "HistManager.h"
#include <iostream>
using namespace std;

double alignment(int stub){
    int cic = (stub-2)/2032;
    double a(0); double b(0);
    if (cic == 1){
        a = 2036.01;
        b = 0.999983;
    }
    else{
        a = -2034.49;
        b = 0.999412;
    }
    // y = a + b*x -> x = ( y - a )/b;
    return (stub - a) /b;
}

vector <TH1*> multi_TH1(HistManager & hm, string name, int nbins, int x0, int x1, int n_hists = 4){
    vector <TH1 *> m_hist;
    for (int i = 0; i < n_hists; i++){
        m_hist.push_back(hm.AddTH1((name+"_"+to_string(i)).c_str(), nbins, x0, x1));
    }
    return m_hist;
}

vector <TH2*> multi_TH2(HistManager & hm, string name, int nbinsX, int x0, int x1, int nbinsY, int y0, int y1){
    vector <TH2 *> m_hist;
    string names[] = {"0_1", "0_2", "0_3", "1_2", "1_3", "2_3"};
    for (int i = 0; i < 6 ;i++){
        m_hist.push_back(hm.AddTH2((name+"_"+names[i]).c_str(), nbinsX, x0, x1, nbinsY, y0, y1));
    }
    return m_hist;
}
vector <TH1*> corr_TH1(HistManager & hm, string name, int nbinsX, int x0, int x1){
    vector <TH1 *> m_hist;
    string names[] = {"0_1", "0_2", "0_3", "1_2", "1_3", "2_3"};
    for (int i = 0; i < 6 ;i++){
        m_hist.push_back(hm.AddTH1((name+"_"+names[i]).c_str(), nbinsX, x0, x1));
    }
    return m_hist;
}



int main(int argc, char ** argv){
    if (argc != 3){
        std::cerr<<"Expecting exactly two arguments !"<<std::endl;
        return 1;
    }

    string input = argv[1];
    string output = argv[2];
    std::cout<<"Input file : "<<input<<" Output file : " << output<<std::endl;    

    HistManager hm(output);
    TreeReader  tr(input);
    
    auto beam_profiles = multi_TH1(hm,"beam_profile",2048,0,4096);
    auto error_bits    = multi_TH1(hm,"error_bits",18,0,18);
    auto stub_persist  = multi_TH1(hm,"stub_persist",10,0,10);
    auto n_stubs       = multi_TH1(hm,"n_stubs",50,0,50);
    auto n_stubs_bx        = multi_TH1(hm,"n_stubs_bx",50,0,50);
    auto n_stubs_bx_CIC_1D = hm.AddTH1("n_stubs_bx_CIC_1D",50,0,50);
    auto n_stubs_bx_CIC_2D = hm.AddTH2("n_stubs_bx_CIC",8,0,8,50,0,50);
    for (int binId = 1; binId < 9; binId++)
        n_stubs_bx_CIC_2D->GetXaxis()->SetBinLabel(binId,("Mod "+to_string((binId-1)/2)+" CIC "+to_string((binId-1)%2)).c_str());
    auto n_stubs_bx_CBC_1D = hm.AddTH1("n_stubs_bx_CBC_1D",50,0,50);
    auto n_stubs_bx_CBC_2D = hm.AddTH2("n_stubs_bx_CBC",4,0.5,4.5,64,0,64);
    for (int binId = 1; binId < 9; binId++)
        n_stubs_bx_CIC_2D->GetXaxis()->SetBinLabel(binId,("Mod "+to_string((binId-1)/2)+" CIC "+to_string((binId-1)%2)).c_str());
        
    auto stub_bend       = multi_TH1(hm,"stub_bend",17,-8,8);

    auto sync          = corr_TH1(hm,"sync",17,-8.5,8.5);

    auto corr          = multi_TH2(hm,"correlation", 512,0,4096,512,0,4096);
    auto n_stubs_corr  = multi_TH2(hm,"n_stubs_corr",50,0,50,50,0,50);
    
    auto total_stubs      = hm.AddTH1("total_stubs",50,0,50);
    auto total_error_bits = hm.AddTH1("total_error_bits",18,0,18);
    auto error_bit_map    = hm.AddTH2("error_bit_map", 4,0,4,18,0,18);

    auto beam_spot_0      = hm.AddTH2("beam_spot_0",512,0,2048,512,0,2048);
    auto beam_spot_1      = hm.AddTH2("beam_spot_1",512,0,2048,512,0,2048);


    // auto n_stubs_bx_CBC_1D = hm.AddTH1("n_stubs_bx_CBC_1D",5,0,5);
    // auto n_stubs_bx_CBC_2D = hm.AddTH2("n_stubs_bx_CBC",5,0,5,64,0,64);

    cout<<tr.getEntries()<<endl;
    int n_stubs_processed = 0;
    int n_events_processed = 0;
    while(tr.getNextMultiEvent()){
        n_events_processed++;
//        if (n_stubs_processed > 1e5){
//            cout<<"I have enough..."<<endl;
//            break;
//        }

        if (n_events_processed%100000 == 0){
            cout<<n_events_processed<<" multi-events, with "<<n_stubs_processed<<" stubs processed."<<endl;
        }

        int total_n_stubs = 0;
        //Single module stuff
    
        for (int modId = 0; modId < 4; modId++){
            //beam profile
            for (auto h0: tr.me.stub_set[modId]){
                beam_profiles[modId]->Fill(h0);
                n_stubs_processed++;        
            }
            for (auto bends: tr.me.bend[modId]){
                for (auto b: bends)
                    stub_bend[modId]->Fill(b);
            }

            //Stub persistance
            int persistance = 0;
            for (uint bx = 0; bx < tr.me.stub[modId].size(); bx++){
                map <int,int> cic_occ;
                map <int,int> cbc_occ;
                if (tr.me.stub[modId].at(bx).size() > 0){
                    persistance++;
                    n_stubs_bx[modId]->Fill(tr.me.stub[modId].at(bx).size());

                    //Split by CIC/ CBC(?)
                    for (auto stub: tr.me.stub[modId].at(bx)){
                        int cic = (stub-2)/2032;
                        int cbc = (stub-2)/254;
                        cic_occ[cic]++;
                        cbc_occ[cbc]++;
                    }
                    for (auto occ : cic_occ){
                        n_stubs_bx_CIC_2D->Fill(2*modId + occ.first,occ.second);
                        n_stubs_bx_CIC_1D->Fill(occ.second);
                    }

                    for (auto occ : cbc_occ){
                        n_stubs_bx_CBC_2D->Fill(occ.second,16*modId + occ.first);
                        n_stubs_bx_CBC_1D->Fill(occ.second);
                    }
                }
            }
            if (persistance > 0)
                stub_persist[modId]->Fill(persistance);
            
            //Number of stubs
            n_stubs[modId]->Fill(tr.me.stub_set[modId].size());
            total_n_stubs+=tr.me.stub_set[modId].size();

            int or_bits = 0;
            for (auto err: tr.me.module_status[modId])
                or_bits|=err;
            if (or_bits != 0){
                for (int bb=0; bb<16; bb++){
                    if ((or_bits>>bb)&1){
                        error_bits[modId]->Fill(bb);
                        total_error_bits->Fill(bb);
                        error_bit_map->Fill(modId,bb);
                    }
                }
            }
        }

        total_stubs->Fill(total_n_stubs);
        

        //Double module stuff
        int corr_index = -1;
        for (int modId = 0; modId < 4; modId++){
            for (int modId2 = modId+1; modId2<4; modId2++){
                corr_index++;
                
                //Beam profile correlation
                for (auto h0: tr.me.stub_set[modId]){
                    for (auto h1: tr.me.stub_set[modId2]){
                        corr[corr_index]->Fill(h0,h1);
                    }
                }

                //Number of stubs correlation
                n_stubs_corr[corr_index]->Fill(tr.me.stub_set[modId].size(),tr.me.stub_set[modId2].size());

                int first_start = -1;
                int second_start = -1;
                for (uint bx = 0; bx < tr.me.stub.at(modId).size(); bx++){
                    if (tr.me.stub.at(modId).at(bx).size() > 0){
                        first_start = bx;
                        break;
                    }
                }
                for (uint bx = 0; bx < tr.me.stub.at(modId).size(); bx++){
                    if (tr.me.stub.at(modId2).at(bx).size() > 0){
                        second_start = bx;
                        break;
                    }
                }
                if (first_start > -1 && second_start > -1)
                    sync[corr_index]->Fill(first_start-second_start);
                
            }
        }
        // Beam profile:
        for (auto h0: tr.me.stub_set[0]){
            for (auto h1: tr.me.stub_set[1]){
                beam_spot_0->Fill(h0%2032,h1%2032);
            }
        }
        for (auto h0: tr.me.stub_set[2]){
            for (auto h1: tr.me.stub_set[3]){
                beam_spot_1->Fill(h0%2032,h1%2032);
            }
        }

    }
    //Getting a few trend information...
    tr.peekFirstEvent();
    float time_start=tr.e.long_bx/4e7;
    
    tr.peekLastEvent();
    float time_stop = tr.e.long_bx/4e7;
    float time=0.5*(time_start+time_stop);
    float duration = time_stop-time_start;
    if (duration != 0 && n_events_processed != 0){
        if (n_stubs_processed > 0)
            hm.AddTrend("trend_stub_rate",n_stubs_processed*1./duration,time);
        if (n_events_processed > 0){
            hm.AddTrend("trend_event_rate",n_events_processed*1./duration,time);
            hm.AddTrend("trend_stub_multiplicity",n_stubs_processed*1./n_events_processed,time);
        }
    }

    if (tr.getEntries() > 20000){
        tr.reset();
        Aligner align(&tr, &hm);
        align.perform_align();
        align.draw_residuals(true);
        align.draw_clean_residuals(true);
    }

    for (int mod_id = 1; mod_id < 4; mod_id++){
        if (sync[mod_id-1]->GetEntries() > 1000)
            hm.AddTrend((string)"trend_sync_0_"+to_string(mod_id),sync[mod_id-1]->GetMean(), time);
    }

    auto sync_2D      = hm.AddTH2("sync",4,0,4,4,0,4);
    int corr_index = -1;
    for (int modId = 0; modId < 4; modId++){
        for (int modId2 = modId+1; modId2<4; modId2++){
            corr_index++;
            sync_2D->SetBinContent(modId, modId2,sync[corr_index]->GetMean());
        }
    }
    

    hm.Write();
    return 0;
}
