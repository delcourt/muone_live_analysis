#include "TreeReader.h"
#include "HistManager.h"
#include <fstream>
#include <iostream>
using namespace std;


double alignment(int stub){
    int cic = (stub-2)/2032;
    double a(0); double b(0);
    if (cic == 1){
        a = 2036.01;
        b = 0.999983;
    }
    else{
        a = -2034.49;
        b = 0.999412;
    }
    return (stub - a) /b;
}

// y = a + b*x -> x = ( y - a )/b;

double inv_alignement(int stub){
    int cic = (stub-2)/2032;
    double a(0); double b(0);
    if (cic == 1){
        a = 2036.01;
        b = 0.999983;
    }
    else{
        a = -2034.49;
        b = 0.999412;
    }
    // y = a + b*x -> x = ( y - a )/b;
    return (b*stub + a);
}



int main(int argc, char ** argv){
    if (argc != 3){
        cout<<"Error, expecting 2 args..."<<endl;
        return 1;
    }
    cout<<argv[0]<<" "<<argv[1]<<" "<<argv[2]<<endl;
    int max_entries = 1e8;
    float skip_val  = 1000;
    int skip_min  = 1000;
    float skip_mult = 1.44;
    int skip_max  = 1e7;
    uint64_t start_time = 30*60*(uint64_t)40000000;
    uint64_t stop_time = 100*60*(uint64_t)40000000;

    set < uint32_t > banned_keys;

    fstream ban_file("vcth_ban.txt");
    string parser;
    cout<<"Banned keys : ";
    while (getline(ban_file,parser)){
        banned_keys.insert(atoi(parser.c_str()));
        cout<<parser<<" ";
    }
    cout<<endl;   
    ban_file.close();


    
    HistManager hm(argv[2]);
    TreeReader  tr(argv[1]);
    
    uint64_t evt_counter = 0;

    map <uint32_t, TH1* > h_kit_length;
    map <uint32_t, TH1* > h_bel_length;

    map <uint32_t, TH1* > h_bel_rate;
    map <uint32_t, TH1* > h_kit_rate;

    map <uint32_t, TH1* > h_bel_profile;
    map <uint32_t, TH1* > h_kit_profile;
    map <uint32_t, map <int, int>> rate_index;
    map <uint32_t ,map <int, int>> beam_status;
    map <uint32_t, TH1* > h_beam_status;


    //Pre start checks : 
    tr.peekFirstEvent();
    uint32_t first_IPbus = tr.e.IPbus_user_data;
    if (tr.e.long_bx > stop_time){
        cout<<"First event after stopping time... Skipping run !"<<endl;
        return 0;
    }
    tr.peekLastEvent();
    if (tr.e.long_bx < start_time){
        cout<<"Last event before starting time... Skipping run !"<<endl;
        return 0;
    }

    if (banned_keys.find(first_IPbus) != banned_keys.end()){
        cout<<"First entry has banned IPbus ("<<first_IPbus<<")";
        if (first_IPbus == tr.e.IPbus_user_data){
            cout<<" and is the same as the last entry. Skipping file."<<endl;
            return 0;
        }
        else{
            cout<<" but is different from the last entry ("<<tr.e.IPbus_user_data<<"). Proceeding"<<endl;
        }
        
    }


    cout<<"Generating rate index..."<<endl;
    while(tr.getNextMultiEvent()){
        evt_counter++;
        if (evt_counter % 100000 == 0){
            cout<<tr.getProgress()<<"\r"<<std::flush;
        }
        int time = tr.me.long_bx[0]/4e7;
        uint32_t IP_val = tr.me.IPbus_user_data.at(0);
        // Only looking at bxl and kit
        // if (tr.me.stub_set[0].size() + tr.me.stub_set[1].size() > 0)
        rate_index[IP_val][time]++;
    }
    cout<<endl;

    int max_rate = 0;
    int t0 = 0;
    int t1 = 0;
    for (auto IP_coup : rate_index){
        for (auto key:IP_coup.second){
            if (t0 == 0 || key.first < t0)
                t0 = key.first;
            if (key.first > t1)
                t1 = key.first;
            if (key.second > max_rate)
                max_rate = key.second;
        }
    }
    tr.reset();
    auto h_rate = hm.AddTH1("evt_rate",1000,0,max_rate);
    auto h_rate_time = hm.AddTH1("evt_rate_time",t1-t0,t0,t1);
    for (auto IP_coup : rate_index){
        for (auto key:IP_coup.second){
            h_rate->Fill(key.second);
            h_rate_time->Fill(key.first, key.second);
        }
    }
    
    // 0 : off
    // 1 : on
    // -1: in-between
    
    float rate_low = 20;
    float rate_high = 100;
    for (auto IP_coup : rate_index){
        for (auto entry :IP_coup.second){
            if (entry.second > rate_high)
                beam_status[IP_coup.first][entry.first] = 1;
            else if (entry.second < rate_low){
                if (rate_index[IP_coup.first][entry.first-1] > rate_low || rate_index[IP_coup.first][entry.first+1] > rate_low)
                    beam_status[IP_coup.first][entry.first] = -1;
                else
                    beam_status[IP_coup.first][entry.first] = 0;
            }
            else{
                beam_status[IP_coup.first][entry.first] = -1;
            }
            h_rate->Fill(entry.second);
            h_rate_time->Fill(entry.first, entry.second);
        }
    }


    /*cout<<"Pre removal : "<<endl;
    for (auto IP_coup: rate_index){
        cout<<IP_coup.first<<" --- > "<<IP_coup.second->begin()->first<<" - " <<IP_coup.second->rbegin()->first<<endl;
        IP_coup.second.erase(IP_coup.second->begin()->first);
        IP_coup.second.erase(IP_coup.second->rbegin()->first);
    }
    */
    for (auto IP_coup: beam_status){
        IP_coup.second.begin()->second = -1;
        IP_coup.second.rbegin()->second = -1;
    }

    evt_counter = 0;
    while(tr.getNextMultiEvent()){
        evt_counter++;
        if (tr.me.IPbus_user_data.size() == 0){
            cout<<endl<<"Warning, no IPbus user data"<<endl;
            continue;
        }

        // Assuming all IP values are the same...
        uint32_t IP_val = tr.me.IPbus_user_data.at(0);
        int time = tr.me.long_bx[0]/4e7;
        //IP_val += (rate_index[time] > 10) <<31;

        if (evt_counter % 100000 == 0){
            cout<<tr.getProgress()<<" "<<IP_val<<endl;            
        }
        // Skipping events outside time window
        if (tr.me.long_bx[0] < start_time || tr.me.long_bx[0] > stop_time)
            continue;
            
        if (beam_status[IP_val][time] != 0)
            continue;

        if (h_kit_length.count(IP_val) == 0){
            string name = "kit_length_"+to_string(IP_val);
            h_kit_length [IP_val] = hm.AddTH1(name,10,0,10);
            name = "bel_length_"+to_string(IP_val);
            h_bel_length [IP_val] = hm.AddTH1(name,10,0,10);

            name = "kit_profile_"+to_string(IP_val);
            h_kit_profile [IP_val] = hm.AddTH1(name,4065,0,4065);
            name = "bel_profile_"+to_string(IP_val);
            h_bel_profile [IP_val] = hm.AddTH1(name,4065,0,4065);
            //Compute total down time:
            int prev_status = -2;
            int prev_time   = 0;
            name = "Beam_status_"+to_string(IP_val);
            auto h_beam_status = hm.AddTH1(name,3,-1.5,1.5);
            for (auto entry:beam_status[IP_val]){
                if (prev_status == -2){
                    prev_status = entry.second;
                    prev_time   = entry.first;
                }
                else{
                    if (prev_status == entry.second)
                        continue;
                    else{
                        h_beam_status->Fill(prev_status, entry.first-prev_time-1);
                        prev_time = entry.first;
                        prev_status = entry.second;
                    }
                }
            }
        }

        if (banned_keys.find(IP_val) != banned_keys.end()){
            //TODO Skip !
            cout<<"Skipping "<<IP_val<<" : "<<skip_val<<" events "<<tr.getProgress()<<endl;            
            bool rc = tr.getEvent(tr.get_event_counter()+skip_val);
            skip_val *= skip_mult;
            if (skip_val > skip_max){
                skip_val = skip_max;
            }
            rc &= tr.getNextMultiEvent();
            if (rc == 0)
                break;
            continue;
        }
        skip_val = skip_min;
        
        for (auto hit0 : tr.me.stub_set[0]){
            h_kit_profile [IP_val]->Fill(hit0);
            break;
        }
        for (auto hit0 : tr.me.stub_set[1]){
            h_bel_profile [IP_val]->Fill(hit0);
            break;
        }

        h_kit_length[IP_val]->Fill(tr.me.stub[0].size());
        h_bel_length[IP_val]->Fill(tr.me.stub[1].size());
      
    }
    
    hm.Write();
    return 0;
}
