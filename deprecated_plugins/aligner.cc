#include "TreeReader.h"
#include "TH1I.h"
#include "TFile.h"
#include "TH2I.h"
#include "TProfile.h"
#include "TGraph.h"
#include <map>
#include <iostream>
using namespace std;






int main(int argc, char ** argv){
    if (argc != 3){
        std::cerr<<"Expecting exactly two arguments !"<<std::endl;
        return 1;
    }
    string input = argv[1];
    string output = argv[2];
    std::cout<<"Input file : "<<input<<" Output file : " << output<<std::endl;    


    TreeReader tr(input);

    map < int, map <float, TH2 * >> correlator; 
    correlator[0] = map<float,TH2 *>();
    correlator[1] = map<float,TH2 *>();
     
    /// Multi-bx container
    vector < map <int, pair < int , int > > > mbm; //Multi-Bx-Monster
    for (int module_id = 0; module_id < 4; module_id ++){
         mbm.push_back( map <int, pair < int , int >> () );
    }
    uint64_t prev_bx = 0;

    while (tr.getEvent()){
        uint64_t bx = tr.e.bx + (tr.e.bx_super_id <<16);
        // cout<<"??"<<endl;
        if (bx != prev_bx + 1){
                //  cout<<"."<<endl;
                // cout<<"Size: "<<mbm.size()<<endl;
                for (auto pp: mbm.at(0)){
                    // cout<<".."<<endl;
                    int bend = pp.second.second / pp.second.first;
                    int cic = (pp.first - 2) / (2032);
                    int stub = pp.first;
                    // cout<<"?"<<endl;
                    if (correlator[cic].find(bend) == correlator[cic].end()){
                        // Create new histogram
                        string name = "correlator_CIC"+to_string(cic)+"_BEND"+to_string(bend);
                        correlator[cic][bend] = new TH2I(name.c_str(),name.c_str(), 0.05*4064,0,4064,0.05*4064,0,4064);
                    }
                    for (auto pp2 : mbm.at(1)){
                        correlator[cic][bend]->Fill(stub,pp2.first);
                    }
                }
                for (int i = 0; i < 2 ; i++)
                    mbm.at(i).clear();
        }

        for (int module_id = 0; module_id < 2; module_id ++){
            // cout<<module_id<<endl;
            for (int i = 0; i < tr.e.stub.at(2*module_id)->size(); i++){
                mbm.at(module_id)[tr.e.stub.at(2*module_id)->at(i)].first++;
                mbm.at(module_id)[tr.e.stub.at(2*module_id)->at(i)].second+=tr.e.bend.at(2*module_id)->at(i);
            }
        } // And I don't care about the last event ><
        
        
        prev_bx = bx;
    }


    TFile * output_ = new TFile(output.c_str(),"RECREATE");
    for (auto mm_1 : correlator){
        for (auto mm_2 : mm_1.second){
            mm_2.second->Write();
        }
    }
    output_->Close();
    return 0;
    //So, actually, we only care about bend 0 events...
    cout<<"Alignment CIC 0 :"<<endl;
    TProfile * px_ = (TProfile *) (correlator[0][0])->ProfileY();
    px_->Fit("pol1","","",2032,4068);
    px_->SetName("Alignment_CIC0");
    px_->Write();
    cout<<"Alignment CIC 1 :"<<endl;
    px_ = (TProfile *) (correlator[1][0])->ProfileY();
    px_->Fit("pol1", "", "", 0, 2032);
    px_->SetName("Alignment_CIC1");
    px_->Write();

    cout<<"Second version of this ..."<<endl;
    float max_1 [correlator[0][0]->GetXaxis()->GetNbins()];
    float max_2 [correlator[1][0]->GetXaxis()->GetNbins()];
    float xx    [correlator[1][0]->GetXaxis()->GetNbins()];
    for (int i = 0; i < correlator[0][0]->GetXaxis()->GetNbins(); i++){
        TH1 * h_0 = correlator[0][0]->ProjectionX("tmp",i+1,i+1);
        TH1 * h_1 = correlator[1][0]->ProjectionX("tmp_2",i+1,i+1);
        max_1[i] = h_0->GetXaxis()->GetBinCenter(h_0->GetMaximumBin());
        max_2[i] = h_1->GetXaxis()->GetBinCenter(h_1->GetMaximumBin());
        xx   [i] = correlator[0][0]->GetXaxis()->GetBinCenter(i+1) ;
    }
    TGraph * ff_1 = new TGraph(correlator[0][0]->GetXaxis()->GetNbins(),xx,max_1);
    ff_1->Fit("pol1","","",2034,4064);
    ff_1->Write("graph_1");
    TGraph * ff_2 = new TGraph(correlator[0][0]->GetXaxis()->GetNbins(),xx,max_2);
    ff_2->Fit("pol1","","",0,2032);
    ff_2->Write("graph_2");
    output_->Close();
 }
 