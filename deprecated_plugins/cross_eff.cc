#include "TreeReader.h"
#include "HistManager.h"
#include <fstream>
#include <iostream>
using namespace std;


double alignment(int stub, int dut){
    int cic = (stub-2)/2032;
    double a(0); double b(0);
    if (cic == 1){
        a = 2036.01;
        b = 0.999983;
    }
    else{
        a = -2034.49;
        b = 0.999412;
    }
    // y = a + b*x -> x = ( y - a )/b;
    if (dut == 0)
        return a + b*stub;
    return (stub - a) /b;
}


int main(int argc, char ** argv){
    if (argc != 3){
        cout<<"Error, expecting 2 args..."<<endl;
        return 1;
    }
    cout<<argv[0]<<" "<<argv[1]<<" "<<argv[2]<<endl;

    
    HistManager hm(argv[2]);
    TreeReader  tr(argv[1]);
    
    uint64_t evt_counter = 0;
    map <uint32_t, EffContainer *> efficiencies;    

    auto h_residuals   = hm.AddTH1("residuals",1001,-100.05,100.05);
    auto h_bend        = hm.AddTH1("bend",201,-100.5,100.5);

    while(tr.getNextMultiEvent()){
        evt_counter++;

        // Assuming all IP values are the same...
        uint32_t IP_val = tr.me.IPbus_user_data.at(0);
        if (evt_counter % 100000 == 0){
            cout<<tr.getProgress()<<endl;            
        }
        // Skipping events outside time window
        if (tr.me.long_bx[0] < start_time || tr.me.long_bx[0] > stop_time)
            continue;

        if (efficiencies.count(IP_val) == 0){
            string name = "eff_bxl_"+to_string(IP_val);
            efficiencies[IP_val] = hm.AddEfficiency(name,512,0,4096);
        }

        if (banned_keys.find(IP_val) != banned_keys.end()){
            //TODO Skip !
            cout<<"Skipping "<<IP_val<<" : "<<skip_val<<" events "<<tr.getProgress()<<endl;            
            bool rc = tr.getEvent(tr.get_event_counter()+skip_val);
            skip_val *= skip_mult;
            if (skip_val > skip_max){
                skip_val = skip_max;
            }
            rc &= tr.getNextMultiEvent();
            if (rc == 0)
                break;
            continue;
        }
        skip_val = skip_min;

        if (tr.me.stub_set[0].size() != 1)
            continue;
        for (auto hit0 : tr.me.stub_set[0]){
            // Check if bend == 0:
            bool good_bend = false;
            for (int bx_counter = 0; bx_counter < tr.me.stub[0].size(); bx_counter++){
                for (int stub_counter = 0; stub_counter < tr.me.stub[0].at(bx_counter).size(); stub_counter++){
                    if (tr.me.stub[0].at(bx_counter).at(stub_counter) == hit0 && tr.me.bend[0].at(bx_counter).at(stub_counter) == 0){
                        good_bend =true; 
                        break;
                    }
                }
            }
            if (good_bend == false)
                continue;

            
            bool matched = false;
            for (auto hit1 : tr.me.stub_set[1]){
                h_residuals->Fill(alignment(hit0)-hit1);
                if (abs(alignment(hit0)-hit1) < 10){
                    matched = true;
                }
                break;
            }
            efficiencies[IP_val]->Fill(alignment(hit0), matched);
        }
        if (efficiencies[IP_val]->get_entries() > 1e6){
            banned_keys.insert(IP_val);
            cout<<endl<<"Banning IP_val "<<IP_val<<endl;
            ofstream ban_file_adder("vcth_ban.txt",std::ios::app);
            ban_file_adder<<IP_val<<endl;
            ban_file_adder.close();
        }
      
    }
    hm.Write();
    return 0;
}