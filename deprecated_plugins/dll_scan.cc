#include "TreeReader.h"
#include "HistManager.h"
#include <iostream>
using namespace std;


double alignment(int stub){
    int cic = (stub-2)/2032;
    double a(0); double b(0);
    if (cic == 1){
        a = 2036.01;
        b = 0.999983;
    }
    else{
        a = -2034.49;
        b = 0.999412;
    }
    // y = a + b*x -> x = ( y - a )/b;
    return (stub - a) /b;
}




int main(int argc, char ** argv){
    if (argc != 3){
        cout<<"Error, expecting 2 args..."<<endl;
        return 1;
    }

    HistManager hm(argv[2]);
    TreeReader  tr(argv[1]);
    uint64_t evt_counter = 0;
    map <uint32_t, EffContainer *> efficiencies;    
    map <uint32_t, TH1 *>          timing_hists;    
    map <uint32_t, TH1 *>          timing_hists_last;    
    map <uint32_t, TH1 *>          timing_bxl_length;    
    map <uint32_t, TH1 *>          timing_kit_length;    
    auto h_size = hm.AddTH1("event_size",20,0,20);
    auto h_correlator = hm.AddTH1("correlator", 100000,0 ,100000);
    auto h_timing = hm.AddTH1("timing", 2*65536+1,-65536.5 ,65536.5);
    auto strange_kit = hm.AddTH1("strange_kit",512,0,4096);
    auto strange_bel = hm.AddTH1("strange_bel",512,0,4096);
    auto bx_dist     = hm.AddTH1("bx_dist",10000,0,10*4294967127);
    auto evt_time    = hm.AddTH1("evt_time",100000,0,100000);
    uint64_t bx_m1(0);
    uint64_t bx_m2(0);
    uint64_t bx_m3(0);
    uint64_t avg(0);
    uint64_t n_evts(0);
    while(tr.getNextMultiEvent()){
        evt_counter++;
        if (evt_counter % 100000 == 0){
            cout<<tr.getProgress()<<"\r"<<std::flush;         
        }
        if (tr.me.IPbus_user_data.size() == 0){
            cout<<"Warning, no IPbus user data"<<endl;
            continue;
        }
        h_size->Fill(tr.me.size());
        // Assuming all IP values are the same...
        uint32_t IP_val = tr.me.IPbus_user_data.at(0);
        if (efficiencies.count(IP_val) == 0){
            string name = "eff_bxl_"+to_string(IP_val);
            efficiencies[IP_val] = hm.AddEfficiency(name,512,0,4096);
            name = "timing_bxl_"+to_string(IP_val);
            timing_hists[IP_val] = hm.AddTH1(name,51,-25.5,25.5);
            name = "timing_bxl_last"+to_string(IP_val);
            timing_hists_last[IP_val] = hm.AddTH1(name,51,-25.5,25.5);
            name = "bxl_length_"+to_string(IP_val);
            timing_bxl_length[IP_val] = hm.AddTH1(name,20,0,20);
            name = "kit_length_"+to_string(IP_val);
            timing_kit_length[IP_val] = hm.AddTH1(name,20,0,20);

        }

        //Timing plots
        if (tr.me.stub_set[0].size() > 0 && tr.me.stub_set[1].size() > 0){
        //if (true){
            //getting first hit of KIT :
            int t_kit_first=0;
            int t_bxl_first=0;
            int t_kit_last=0;
            int t_bxl_last=0;
            int bx_id = 0;
            for (auto bx: tr.me.stub[0]){
                if (bx.size() > 0 ){
                    auto t = tr.me.bx.at(bx_id);
                    if (t_kit_first == 0 || t < t_kit_first)
                        t_kit_first = t;
                    if (t_kit_last == 0 || t > t_kit_last) 
                        t_kit_last = t;
                }
                bx_id++;
            }
            bx_id = 0;
            for (auto bx: tr.me.stub[1]){
                if (bx.size() > 0 ){
                    auto t = tr.me.bx.at(bx_id);
                    if (t_bxl_first == 0 || t < t_bxl_first)
                        t_bxl_first = t;
                    if (t_bxl_last == 0 || t > t_bxl_last) 
                        t_bxl_last = t;
                }
                bx_id++;
            }
            timing_hists[IP_val]->Fill((int)(t_bxl_first-t_kit_first));
            timing_hists_last[IP_val]->Fill((int)(t_bxl_last-t_kit_last));
            timing_bxl_length[IP_val]->Fill((int)(t_bxl_last-t_bxl_first+1));
            timing_kit_length[IP_val]->Fill((int)(t_kit_last-t_kit_first+1));
        }

        //Efficiency plots
        for (auto hit0 : tr.me.stub_set[0]){
            bool matched = false;
            for (auto hit1 : tr.me.stub_set[1]){
                if (abs(alignment(hit0)-hit1) < 5){
                    matched = true;
                }
                break;
            }
            efficiencies[IP_val]->Fill(hit0, matched);
        }
        if (tr.me.stub.at(0).size() > 0 || tr.me.stub.at(1).size() > 0){
            h_correlator->Fill(tr.me.long_bx.at(0) - bx_m1);
            //h_correlator->Fill(tr.me.long_bx.at(0) - bx_m2);
            //h_correlator->Fill(tr.me.long_bx.at(0) - bx_m3);
            for (auto x : tr.me.bx){
                h_timing->Fill(x);
            }
            uint64_t dt = tr.me.long_bx.at(0) - bx_m1;
            if (dt > 919 && dt < 930){
                for (auto s: tr.me.stub_set.at(0))
                    strange_kit->Fill(s);
                for (auto s: tr.me.stub_set.at(1))
                    strange_bel->Fill(s);
            }
            if (avg < bx_m1)
                avg = bx_m1;
            bx_dist->Fill(bx_m1);
            evt_time->Fill(bx_m1*1./4e7);
            //cout<<(uint64_t) avg<<"-"<<bx_m1<<endl;
            n_evts++;
            bx_m1 = tr.me.long_bx.at(0);
            bx_m2 = bx_m1; 
            bx_m3 = bx_m2;
        }
    }
    cout<<"Salut"<<endl<<endl;
    cout<<avg*1./n_evts<<endl;
    hm.Write();
    return 0;
}