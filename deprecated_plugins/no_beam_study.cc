#include "TreeReader.h"
#include "HistManager.h"
#include <fstream>
#include <iostream>
using namespace std;


double alignment(int stub){
    int cic = (stub-2)/2032;
    double a(0); double b(0);
    if (cic == 1){
        a = 2036.01;
        b = 0.999983;
    }
    else{
        a = -2034.49;
        b = 0.999412;
    }
    return (stub - a) /b;
}

// y = a + b*x -> x = ( y - a )/b;

double inv_alignement(int stub){
    int cic = (stub-2)/2032;
    double a(0); double b(0);
    if (cic == 1){
        a = 2036.01;
        b = 0.999983;
    }
    else{
        a = -2034.49;
        b = 0.999412;
    }
    // y = a + b*x -> x = ( y - a )/b;
    return (b*stub + a);
}

TH1 * hist_getter(int id, const pair <TH1*, TH1*> & histograms){
    if (id == 0)
        return histograms.first;
    else
        return histograms.second;
}

vector<pair < TH1*, TH1*>> generate_hists(HistManager & hm, string name, int n_bins, float x0, float x1){
    vector <string> suffixes;
    suffixes.push_back("_kit");
    suffixes.push_back("_bel");
    suffixes.push_back("_per0");
    suffixes.push_back("_per1");    

    vector<pair < TH1*, TH1*>> histograms;
    for (int i = 0; i < 4; i++){
        histograms.push_back(make_pair(hm.AddTH1(name+"_no_beam"+suffixes[i],n_bins,x0,x1),hm.AddTH1(name+"_beam"+suffixes[i],n_bins,x0,x1)));
    }
    return histograms;
}


int main(int argc, char ** argv){
    if (argc != 3){
        cout<<"Error, expecting 2 args..."<<endl;
        return 1;
    }
    cout<<argv[0]<<" "<<argv[1]<<" "<<argv[2]<<endl;

    HistManager hm(argv[2]);
    TreeReader  tr(argv[1]);
    
    uint64_t evt_counter = 0;

    map <uint32_t, EffContainer *> efficiencies;    
    map <uint32_t, TH1* > h_kit_length;
    map <uint32_t, TH1* > h_kit_length_unseen;
    map <uint32_t, TH1* > h_kit_bel_dt;

    //Generate rate index
    cout<<"Generating rate index..."<<endl;
    map <int, int> rate_index;
    while(tr.getNextMultiEvent()){
        evt_counter++;
        if (evt_counter % 100000 == 0){
            cout<<tr.getProgress()<<"\r"<<std::flush;
        }
        int time = tr.me.long_bx[0]/4e7;
        // Only looking at bxl and kit
        // if (tr.me.stub_set[0].size() + tr.me.stub_set[1].size() > 0)
        rate_index[time]++;
    }
    cout<<endl;
    int max_rate = 0;
    int t0 = 0;
    int t1 = 0;
    for (auto key:rate_index){
        if (t0 == 0 || key.first < t0)
            t0 = key.first;
        if (key.first > t1)
            t1 = key.first;
        if (key.second > max_rate)
            max_rate = key.second;
    }
    tr.reset();

    
    auto h_n_stubs_corr = hm.AddTH2("n_stubs_corr",20,0,20,20,0,20);
    auto h_n_stubs_corr_no_beam = hm.AddTH2("n_stubs_corr_no_beam",20,0,20,20,0,20);

    auto h_rate = hm.AddTH1("evt_rate",1000,0,max_rate);
    auto h_rate_time = hm.AddTH1("evt_rate_time",t1-t0,t0,t1);
    // 0 : off
    // 1 : on
    // -1: in-between
    map <int, int> beam_status;
    float rate_low = 20;
    float rate_high = 100;
    for (auto entry :rate_index){
        if (entry.second > rate_high)
            beam_status[entry.first] = 1;
        else if (entry.second < rate_low){
            if (rate_index[entry.first-1] > rate_low || rate_index[entry.first+1] > rate_low)
                beam_status[entry.first] = -1;
            else
                beam_status[entry.first] = 0;
        }
        else{
            beam_status[entry.first] = -1;
        }
        h_rate->Fill(entry.second);
        h_rate_time->Fill(entry.first, entry.second);
    }
    //Compute total down time:
    int prev_status = -2;
    int prev_time   = 0;
    auto h_beam_status = hm.AddTH1("Beam_status",3,-1.5,1.5);
    for (auto entry:beam_status){
        if (prev_status == -2){
            prev_status = entry.second;
            prev_time   = entry.first;
        }
        else{
            if (prev_status == entry.second)
                continue;
            else{
                h_beam_status->Fill(prev_status, entry.first-prev_time-1);
                prev_time = entry.first;
                prev_status = entry.second;
            }
        }
    }

    cout<<"Filling histograms..."<<endl;
    evt_counter = 0;
    auto h_residuals   = make_pair(hm.AddTH1("residuals_no_beam",1001,-100.05,100.05),hm.AddTH1("residuals_beam",1001,-100.05,100.05));


    auto h_n_stubs     = generate_hists(hm,"n_stubs",20,0,20);
    auto h_stub_profile= generate_hists(hm,"stub_profile",4096,0,4096);
    auto h_stub_bend   = generate_hists(hm,"stub_bend",21,-10.5,10.5);


    while(tr.getNextMultiEvent()){
        evt_counter++;
        if (evt_counter % 100000 == 0){
            cout<<tr.getProgress()<<"\r"<<std::flush;
        }

        int time = tr.me.long_bx[0]/4e7;
        if (beam_status[time] == -1)
            continue;
        
        //Removing events with hits only on Perugia modules
        // if (tr.me.stub_set[0].size() + tr.me.stub_set[1].size() == 0)
        //     continue;

        bool beam_on = (beam_status[time] == 1);

        
        for (int detId = 0; detId < 4; detId ++){
            hist_getter(beam_on,h_n_stubs[detId])->Fill(tr.me.stub_set[detId].size());
            for (auto hit : tr.me.stub_set[detId])
                hist_getter(beam_on,h_stub_profile[detId])->Fill(hit);
            for (auto bend_vec: tr.me.bend[detId])
                for (auto bend:bend_vec)
                    hist_getter(beam_on,h_stub_bend[detId])->Fill(bend);
        }


        h_n_stubs_corr->Fill(tr.me.stub_set[0].size(),tr.me.stub_set[1].size());
        if(!beam_on)
            h_n_stubs_corr_no_beam->Fill(tr.me.stub_set[0].size(),tr.me.stub_set[1].size());

        for (auto hit1 : tr.me.stub_set[1]){
            for (auto hit0 : tr.me.stub_set[0]){
                hist_getter(beam_on,h_residuals)->Fill(alignment(hit0)-hit1);
            }
        }      


        if (efficiencies.count(beam_on) == 0){
            string name = "eff_bxl_"+to_string(beam_on);
            efficiencies[beam_on] = hm.AddEfficiency(name,512,0,4096);
            name = "kit_length_"+to_string(beam_on);
            h_kit_length [beam_on] = hm.AddTH1(name,10,0,10);
            name = "kit_length_unseen"+to_string(beam_on);
            h_kit_length_unseen [beam_on] = hm.AddTH1(name,10,0,10);
        }

        for (auto hit0 : tr.me.stub_set[0]){
            // Check if bend == 0:
            bool good_bend = false;
            int kit_length = 0;

            for (unsigned int bx_counter = 0; bx_counter < tr.me.stub[0].size(); bx_counter++){
                for (unsigned int stub_counter = 0; stub_counter < tr.me.stub[0].at(bx_counter).size(); stub_counter++){
                    if (tr.me.stub[0].at(bx_counter).at(stub_counter) == hit0){
                        if (tr.me.bend[0].at(bx_counter).at(stub_counter) == 0)
                            good_bend =true; 
                        
                        kit_length++;
                    }
                    
                }
            }
            if (good_bend == false)
                continue;

            h_kit_length[beam_on]->Fill(kit_length);
            
            bool matched = false;
            for (auto hit1 : tr.me.stub_set[1]){
                if (abs(alignment(hit0)-hit1) < 10){
                    matched = true;
                }
                break;
            }
            if (matched == false){
                h_kit_length_unseen[beam_on]->Fill(kit_length);
            }

            efficiencies[beam_on]->Fill(alignment(hit0), matched);
        }      
    }
    cout<<endl;
    auto noise_kit = hm.AddTH1("noise_kit", 50,0,50);
    auto noise_bel = hm.AddTH1("noise_bel", 50,0,50);
    auto noise_per0 = hm.AddTH1("noise_per0", 50,0,50);
    auto noise_per1 = hm.AddTH1("noise_per1", 50,0,50);
    for (int i = 1; i <=4067; i++){
        noise_bel->Fill(hist_getter(0,h_stub_profile[1])->GetBinContent(i));
        noise_kit->Fill(hist_getter(0,h_stub_profile[0])->GetBinContent(i));
        noise_per0->Fill(hist_getter(0,h_stub_profile[2])->GetBinContent(i));
        noise_per1->Fill(hist_getter(0,h_stub_profile[3])->GetBinContent(i));
    }
    hm.Write();
    return 0;
}