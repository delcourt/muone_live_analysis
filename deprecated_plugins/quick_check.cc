 #include "TreeReader.h"
 #include "SkimReader.h"
 #include "TH1I.h"
 #include "TFile.h"
 #include "TH2I.h"

#include <map>
#include <iostream>
using namespace std;





int main(int argc, char ** argv){
    if (argc != 2){
        std::cerr<<"Expecting exactly one argument !"<<std::endl;
        return 1;
    }
    string input = argv[1];
    std::cout<<"Input file : "<<input<<std::endl;    

    TreeReader tr(input);
    std::cout<<"Loaded : "<<input<<std::endl;    
    int entry_id = 0;
     while (tr.getNextMultiEvent()){
        entry_id++;
        auto se = SkimmedEvent(tr.me);
        // Checking content of Skimmed event:
        cout<<endl<<endl<<"New event! Bx = "<<se.long_bx<<" & IP = "<<se.IPbus_user_data<<endl;
        for (int moduleId = 0; moduleId < 4; moduleId ++){
            cout<<"Module # "<<moduleId<<endl;
            for (auto stub : se.stubs[moduleId]){
                cout<<stub.first <<" ";
                for (auto bc: stub.second)
                    cout<<(int)bc<<" ";
                cout<<endl;
            }
            cout<<endl;

        }

        // Looking at the underlying Multi event:
        cout<<"Multi event:"<<endl;
        
        for (int moduleId = 0; moduleId < 4; moduleId ++){
            cout<<"Module # "<<moduleId<<endl;
            for (int bxId = 0; bxId < tr.me.stub[moduleId].size(); bxId++){
                cout<< "bx "<< tr.me.long_bx[bxId]<<endl;
                for (int stubId = 0; stubId < tr.me.stub[moduleId][bxId].size(); stubId++){
                    cout<<tr.me.stub[moduleId][bxId][stubId]<< " " << tr.me.bend_code[moduleId][bxId][stubId]<<endl;
                }
            }
        }

        if (entry_id > 1000)
            break;
     }

 }
 
