#include "TreeReader.h"
#include "HistManager.h"
#include <iostream>
using namespace std;

double alignment(int stub){
    int cic = (stub-2)/2032;
    double a(0); double b(0);
    if (cic == 1){
        a = 2036.01;
        b = 0.999983;
    }
    else{
        a = -2034.49;
        b = 0.999412;
    }
    // y = a + b*x -> x = ( y - a )/b;
    return (stub - a) /b;
}

int main(int argc, char ** argv){

    HistManager hm("test.root");
    TreeReader  tr("/eos/experiment/mu-e/daq/ntuples/merged/run_0046.root");
    auto h_kit_profile = hm.AddTH1("beam_profile_kit",2048,0,4096);
    auto h_bel_profile = hm.AddTH1("beam_profile_bel",2048,0,4096);
    auto h_residuals   = hm.AddTH1("residuals",101,-100.5,100.5);
    auto eff1 = hm.AddEfficiency("eff_bxl",512,0,4096);
    cout<<tr.getEntries()<<endl;
    while(tr.getNextMultiEvent()){
        for (auto hit0 : tr.me.stub_set[0]){
            h_kit_profile->Fill(hit0);
            bool matched = false;
            for (auto hit1 : tr.me.stub_set[2]){
                h_residuals->Fill(alignment(hit0)-hit1);
                if (abs(alignment(hit0)-hit1) < 5){
                    matched = true;
                }
                break;
            }
            eff1->Fill(hit0, matched);
        }      
        for(auto hit1 : tr.me.stub_set[1])         
            h_bel_profile->Fill(hit1);
    }
    auto ee = eff1->GetEff();
    cout<<"Efficiency : "<<100*ee.first<<" +- "<<100*ee.second<<endl;
    hm.Write();
    return 0;
}