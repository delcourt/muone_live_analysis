#include "TreeReader.h"
#include "HistManager.h"
#include <fstream>
#include <iostream>
using namespace std;


double alignment(int stub){
    int cic = (stub-2)/2032;
    double a(0); double b(0);
    if (cic == 1){
        a = 2036.01;
        b = 0.999983;
    }
    else{
        a = -2034.49;
        b = 0.999412;
    }
    return (stub - a) /b;
}

// y = a + b*x -> x = ( y - a )/b;

double inv_alignement(int stub){
    int cic = (stub-2)/2032;
    double a(0); double b(0);
    if (cic == 1){
        a = 2036.01;
        b = 0.999983;
    }
    else{
        a = -2034.49;
        b = 0.999412;
    }
    // y = a + b*x -> x = ( y - a )/b;
    return (b*stub + a);
}

int main(int argc, char ** argv){
    if (argc != 3){
        cout<<"Error, expecting 2 args..."<<endl;
        return 1;
    }
    cout<<argv[0]<<" "<<argv[1]<<" "<<argv[2]<<endl;
    int max_entries = 1e12; //was 1e8
    float skip_val  = 1000;
    int skip_min  = 1000;
    float skip_mult = 1.44;
    int skip_max  = 1e7; 
    uint64_t start_time = 30*60*(uint64_t)40000000;
    uint64_t stop_time = 100*60*(uint64_t)40000000;

    set < uint32_t > banned_keys;

    fstream ban_file("vcth_ban.txt");
    string parser;
    cout<<"Banned keys : ";
    while (getline(ban_file,parser)){
        banned_keys.insert(atoi(parser.c_str()));
        cout<<parser<<" ";
    }
    cout<<endl;   
    ban_file.close();


    
    HistManager hm(argv[2]);
    TreeReader  tr(argv[1]);
    cout<<tr.getEntries()<<endl;
    
    uint64_t evt_counter = 0;
    map <uint32_t, EffContainer *> efficiencies;    
    map <uint32_t, TH1* > h_kit_length;
    map <uint32_t, TH1* > h_kit_length_unseen;
    map <uint32_t, TH1* > h_kit_bel_dt;

    //Pre start checks : 
    tr.peekFirstEvent();
    uint32_t first_IPbus = tr.e.IPbus_user_data;
    if (tr.e.long_bx > stop_time){
        cout<<"First event after stopping time... Skipping run !"<<endl;
        return 0;
    }
    tr.peekLastEvent();
    if (tr.e.long_bx < start_time){
        cout<<"Last event before starting time... Skipping run !"<<endl;
        return 0;
    }

    if (banned_keys.find(first_IPbus) != banned_keys.end()){
        cout<<"First entry has banned IPbus ("<<first_IPbus<<")";
        if (first_IPbus == tr.e.IPbus_user_data){
            cout<<" and is the same as the last entry. Skipping file."<<endl;
            return 0;
        }
        else{
            cout<<" but is different from the last entry ("<<tr.e.IPbus_user_data<<"). Proceeding"<<endl;
        }
        
    }

    auto h_residuals   = hm.AddTH1("residuals",1001,-100.05,100.05);
    
/*    auto h_kit_length         = hm.AddTH1("kit_length"        ,10,0,10);
    auto h_kit_length_unseen  = hm.AddTH1("kit_length_unseen" ,10,0,10);
    auto h_kit_bel_dt         = hm.AddTH1("kit_bel_dt"        ,21,-10.5,10.5);*/

    while(tr.getNextMultiEvent()){
        evt_counter++;
        if (tr.me.IPbus_user_data.size() == 0){
            cout<<endl<<"Warning, no IPbus user data"<<endl;
            continue;
        }

        // Assuming all IP values are the same...
        uint32_t IP_val = tr.me.IPbus_user_data.at(0);
        if (evt_counter % 100000 == 0){
            cout<<tr.getProgress()<<" "<<IP_val<<endl;            
        }
        // Skipping events outside time window
        if (tr.me.long_bx[0] < start_time || tr.me.long_bx[0] > stop_time)
            continue;

        if (efficiencies.count(IP_val) == 0){
            string name = "eff_bxl_"+to_string(IP_val);
            efficiencies[IP_val] = hm.AddEfficiency(name,2048,0,4096);

            name = "kit_length_"+to_string(IP_val);
            h_kit_length [IP_val] = hm.AddTH1(name,10,0,10);
            name = "kit_length_unseen"+to_string(IP_val);
            h_kit_length_unseen [IP_val] = hm.AddTH1(name,10,0,10);
        }

        if (banned_keys.find(IP_val) != banned_keys.end()){
            //TODO Skip !
            cout<<"Skipping "<<IP_val<<" : "<<skip_val<<" events "<<tr.getProgress()<<endl;            
            bool rc = tr.getEvent(tr.get_event_counter()+skip_val);
            skip_val *= skip_mult;
            if (skip_val > skip_max){
                skip_val = skip_max;
            }
            rc &= tr.getNextMultiEvent();
            if (rc == 0)
                break;
            continue;
        }
        skip_val = skip_min;

        if (tr.me.stub_set[0].size() != 1)
            continue;


        for (auto hit0 : tr.me.stub_set[0]){
            // Check if bend == 0:
            bool good_bend = false;
            int kit_length = 0;

            for (int bx_counter = 0; bx_counter < tr.me.stub[0].size(); bx_counter++){
                for (int stub_counter = 0; stub_counter < tr.me.stub[0].at(bx_counter).size(); stub_counter++){
                    if (tr.me.stub[0].at(bx_counter).at(stub_counter) == hit0){
                        if (tr.me.bend[0].at(bx_counter).at(stub_counter) == 0)
                            good_bend =true; 
                        
                        kit_length++;
                    }
                    
                }
            }
            if (good_bend == false)
                continue;

            h_kit_length[IP_val]->Fill(kit_length);
            
            bool matched = false;
            for (auto hit1 : tr.me.stub_set[1]){
                h_residuals->Fill(alignment(hit0)-hit1);
                if (abs(alignment(hit0)-hit1) < 10){
                    matched = true;
                }
                break;
            }
            if (matched == false){
                h_kit_length_unseen[IP_val]->Fill(kit_length);
            }

            efficiencies[IP_val]->Fill(alignment(hit0), matched); 
        }
        if (efficiencies[IP_val]->get_entries() > 1e10){
            banned_keys.insert(IP_val);
            cout<<endl<<"Banning IP_val "<<IP_val<<endl;
            ofstream ban_file_adder("vcth_ban.txt",std::ios::app);
            ban_file_adder<<IP_val<<endl;
            ban_file_adder.close();
        }
      
    }
    hm.Write();
    return 0;
}
