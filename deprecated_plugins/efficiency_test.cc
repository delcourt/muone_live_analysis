#include "TreeReader.h"
#include "TH1I.h"
#include "TFile.h"
#include "TH2I.h"
#include "TProfile.h"
#include <map>
#include <iostream>
#include <cmath>
using namespace std;

double alignment(int stub){
    int cic = (stub-2)/2032;
    double a(0); double b(0);
    if (cic == 1){
        a = 2036.01;
        b = 0.999983;
    }
    else{
        a = -2034.49;
        b = 0.999412;
    }
    // y = a + b*x -> x = ( y - a )/b;
    return (stub - a) /b;
}

double simp_align(int stub){
    int cic = (stub-2)/2032;
    if (cic == 0){
        return stub + 2032;
    }
    else{
        return stub  - 2032;
    }

}

int main(int argc, char ** argv){
    if (argc != 3){
        std::cerr<<"Expecting exactly two arguments !"<<std::endl;
        return 1;
    }
    string input = argv[1];
    string output = argv[2];
    std::cout<<"Input file : "<<input<<" Output file : " << output<<std::endl;    


    TreeReader tr(input);

    map < int, map <float, TH2 * >> correlator; 
    correlator[0] = map<float,TH2 *>();
    correlator[1] = map<float,TH2 *>();
     
    /// Multi-bx container
    vector < map <int, pair < int , int > > > mbm; //Multi-Bx-Monster
    for (int module_id = 0; module_id < 4; module_id ++){
         mbm.push_back( map <int, pair < int , int >> () );
    }
    uint64_t prev_bx = 0;

    TH1 * res = new TH1I("Residuals", "Residuals", 21 ,-10.5,10.5);
    TH1 * res_2 = new TH1I("Residuals_2", "Residuals_2", 21,-10.5,10.5);

    TH1 * seed = new TH1F("Seed","Seed", 8*254, 0, 16*254);
    TH1 * match = new TH1F("Match","Match", 8*254, 0, 16*254);

    while (tr.getEvent()){
        uint64_t bx = tr.e.bx + (tr.e.bx_super_id <<16);
        if (bx != prev_bx + 1){
                for (auto pp: mbm.at(0)){
                    if (pp.second.second / pp.second.first != 0)
                        continue;
                    double expected_stub = alignment(pp.first);
                    seed->Fill(expected_stub);
                    for (auto pp2 : mbm.at(1)){
                        int stub = pp2.first;
                        res->Fill((stub-expected_stub)/2);
                        res_2->Fill((stub-simp_align(pp.first))/2);
                    }
                    for (auto pp2 : mbm.at(1)){
                        int stub = pp2.first;
                        if (abs(stub-expected_stub) < 10){
                            match->Fill(expected_stub);
                            break;
                        }
                    }
                }
                for (int i = 0; i < 2 ; i++)
                    mbm.at(i).clear();
        }

        for (int module_id = 0; module_id < 2; module_id ++){
            for (int i = 0; i < tr.e.stub.at(module_id)->size(); i++){
                mbm.at(module_id)[tr.e.stub.at(module_id)->at(i)].first++;
                mbm.at(module_id)[tr.e.stub.at(module_id)->at(i)].second+=tr.e.bend.at(module_id)->at(i);
            }
        } // And I don't care about the last event ><
        
        
        prev_bx = bx;
    }


    TFile * output_ = new TFile(output.c_str(),"RECREATE");
    

    res->Write();
    res_2->Write();
    seed->Write();
    match->Write();
    TH1 * eff = (TH1*) seed->Clone();
    for (int i = 0; i < seed->GetXaxis()->GetNbins(); i++){
        float num = match->GetBinContent(i+1);
        float den = seed->GetBinContent(i+1);
        if (den == 0){
            eff->SetBinContent(i+1,0);
            eff->SetBinError(i+1,0);
        }
        else{
            double e = num*1./den;
            eff->SetBinContent(i+1,e);
            eff->SetBinError(i+1,sqrt(e*(1-e)/den));
        }
    }
    //So, actually, we only care about bend 0 events...
    eff->SetTitle("Stub efficiency estimate");
    eff->SetName("Stub_efficiency_estimate");
    eff->Write();
    output_->Close();
 }
 
