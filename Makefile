
_DEPS = TreeReader.h HistManager.h Aligner.h SkimReader.h Linkef.h FastReader.h
_OBJ = TreeReader.o HistManager.o Aligner.o SkimReader.o Dict.o FastReader.o

IDIR = ./include
LDIR = ./lib
ODIR = ./src
PDIR = ./plugins
BDIR = ./bin


PLUGINS = $(patsubst %.cc,%.out,$(patsubst plugins/%,$(BDIR)/%,$(wildcard plugins/*.cc)))
DICTC = Dict.cc
DICTH = Dict.h

HDRS_DICT = include/Linkdef.h

CFLAGS = -I$(IDIR) -I./ 
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))
OBJ  = $(patsubst %,$(ODIR)/%,$(_OBJ))
CC = g++
COpt = -Wall -Wextra  `root-config --cflags --libs` -O3



all:
	@echo "############# COMPILING ALL #############"
	@make cint
	@echo "###### COMPILING DEPENDENCIES..."
	@make  -j4 $(OBJ)
	@echo "###### COMPILING $(PLUGINS)..."
	@make -j8 $(PLUGINS)

%.o: %.cc
	$(CC) -c -o $@ $< $(CFLAGS) $(ROOTLIBS) $(COpt) 

cint: $(ODIR)/$(DICTC)

$(ODIR)/$(DICTC): $(HDRS_DICT)
	rootcling -f $@ -c $(CFLAGS) -p $^ 
	@mv src/Dict_rdict.pcm ./bin/


bin/%.out: plugins/%.cc $(OBJ)
	$(CC)  -o $@ $^ $(CFLAGS) $(ROOTLIBS) $(COpt)


.PHONY: clean

clean:
	rm -rf $(ODIR)/*.o hellomake src/Dict.cc include/Dict.h bin/* plugins/*.out

  
