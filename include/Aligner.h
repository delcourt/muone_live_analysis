#pragma once
#include <string>
#include <vector>
#include "TreeReader.h"
#include "HistManager.h"
#include "TFile.h"


class Aligner{
    public:
        bool   is_valid(float x, float y);
        bool r_is_valid(float x, float y);
        float     get_x(float x, float y);
        float   r_get_x(float x, float y);
        float     get_y(float x, float y);
        float   r_get_y(float x, float y);

        Aligner(TreeReader * tt, HistManager * hm_);
        Aligner(std::string input_file);
        Aligner();
        ~Aligner();

        bool load_align (std::string input_file="");
        bool store_align(std::string output_file);
        bool load_tree(TreeReader * tt);

        bool perform_align();
        bool draw_residuals(bool all = false);
        bool draw_clean_residuals(bool all = false);

        bool has_good_bend(int module, int stub, int bend_code);

        void force_good_bend(bool status);
        
        std::vector <int> good_bend_codes;

    private:
        TreeReader * t;
        HistManager * hm;
        TF1 * align_x;
        TF1 * align_y;
        TF1 * r_align_x;
        TF1 * r_align_y;
        bool params_loaded;
        bool good_bend_only;
        int  r_good_bend_code_x;
        int  r_good_bend_code_y;



};