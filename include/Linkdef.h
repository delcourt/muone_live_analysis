#pragma once

#include <TROOT.h>
#include <vector>
#include <map>
#include <string>
#include <utility>
#include <set>
#include "SkimReader.h"
#include "FastReader.h"
#include "TreeReader.h"
#ifdef __MAKECINT__
// #pragma link off all globals;
// #pragma link off all classes;
// #pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;
// #pragma link C++ class std::set<int>+;
#pragma link C++ class SkimmedEvent+;
#pragma link C++ class FastMultiEvent+;
#pragma link C++ class stub_info+;
//#pragma link C++ class std::map<std::string,int>+;
//#pragma link C++ class std::set <uint8_t>+;
// #pragma link C++ class std::map < uint16_t,std::set <uint8_t>>+;
//#pragma link C++ class std::vector < std::map < uint16_t,std::set <uint8_t> > >+;


#endif