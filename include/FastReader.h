#include <vector>
#include <map>
#include <set>
#include "TreeReader.h"
#pragma once


struct small_stub{
    uint16_t    add;
    int8_t      bend;
    bool        CIC;            // <-- Asked by Ali
    uint8_t     bx_offset;      
};

struct FastMultiEvent{
    std::vector < std::vector <small_stub> >  full_stub;
    uint64_t    long_bx;
};


class FastReader{
    private:
        TTree * _tt;
        TFile * _ff;
        uint64_t        event_counter; 
        uint64_t        n_entries;

    public:
        FastReader(std::string file_name);
        ~FastReader();
        void          peekLastEvent();
        void          peekFirstEvent();
        int           getEvent(int event_number = -1);
        int           get_event_counter();
        uint64_t      getEntries();
        std::string   getProgress();

        FastMultiEvent * me_ref;
        FastMultiEvent  me;
};
