#include "TreeReader.h"
#include <vector>
#include <map>
#include <set>
#pragma once
class SkimmedEvent{
    public:
        SkimmedEvent();
        SkimmedEvent(const MultiEvent & me);
        ~SkimmedEvent();
        void LoadMultiEvent(const MultiEvent & me);
        void Reset();

        //Data:
        std::vector < std::map < uint16_t,std::set <uint8_t> > >         stubs;
        uint64_t                                                         long_bx;
        uint32_t                                                         IPbus_user_data;


    private:
        
        //Nothing yet...
};

class SkimReader{
    private:
        TTree * _tt;
        TFile * _ff;
        uint64_t        event_counter; 
        uint64_t        n_entries;

    public:
        SkimReader(std::string file_name);
        ~SkimReader();
        void          peekLastEvent();
        void          peekFirstEvent();
        int           getEvent(int event_number = -1);
        int           get_event_counter();
        uint64_t      getEntries();
        std::string   getProgress();

        SkimmedEvent  *e;
};
