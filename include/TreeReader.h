#pragma once
#include <iostream>
#include <vector>
#include <map>
#include <set>
#include "TTree.h"
#include "TFile.h"

struct stub_info{
    int      add;
    int      bend;           // <-- Decoded bend code.
    int      bend_code;      
    bool     CIC;
    uint64_t long_bx;
 };


struct event{
    event();
    ~event();

    std::vector < std::vector <int> *>       stub;
    std::vector < std::vector <int> *>       bendCode;
    std::vector < std::vector <int> *>       bend;
    std::vector < std::vector <stub_info> >  full_stub;
    uint16_t module_status_0;
    uint16_t module_status_1;
    uint16_t module_status_2;
    uint16_t module_status_3;
    uint16_t bx;
    uint32_t bx_super_id;
    uint32_t IPbus_user_data;
    uint64_t long_bx;
};

class MultiEvent{
    public:
        MultiEvent(bool pre_process = false, bool keep_all = false);
        ~MultiEvent();
        void AddEvent(const event & e);
        void Reset();
        std::vector < std::vector < std::vector <int> > >       stub;
        std::vector < std::vector < std::vector <int> > >       bend;
        std::vector < std::vector < std::vector <int> > >       bend_code;
        std::vector < std::set < int > >                        stub_set;
        std::vector < std::vector <stub_info> >                 full_stub;
        std::vector < std::vector < uint16_t > >                module_status;
        std::vector < uint64_t >                                long_bx;
        std::vector < int >                                     bx;
        std::vector < uint32_t >                                IPbus_user_data;
        unsigned int size();
        void set_filter_on_bend(bool flag, int bend = 0);
        std::set < float > get_bends      (int module, int stub) const;
        std::set < uint8_t > get_bend_codes (int module, int stub) const;

    private:
        bool pre_process_, keep_all_;
        bool set_computed;
        bool            bend_flag;
        int             bend_filter;

};

class TreeReader{
    private:
    TFile * _ff;
    TTree * _tt;
    uint64_t        event_counter; 
    uint64_t        n_entries;
    unsigned int    max_event_buff;
    unsigned int    max_gap;
    bool            event_filled;


    public:
    
    TreeReader(std::string file_name);
    ~TreeReader();
    void     peekLastEvent();
    void     peekFirstEvent();
    void     reset();
    void    process_event();
    int     getNextMultiEvent();
    int      getEvent(int event_number = -1);
    int     get_event_counter();
    uint64_t getEntries();
    std::string   getProgress();

    event   e;
    MultiEvent me;
};
