from ROOT import TFile, TH1I, TH2I, TCanvas, gROOT

gROOT.SetBatch(1)

LUT = [0, 1, 2, 3, 4, 5, 6, 7, 999 ,-1,-2,-3,-4,-5,-6,-7]
cic_mapper = [0,1,2,3,7,6,5,4]


def bend(bend_code):
    return LUT[bend_code]

def alignment(stub):
    cic = (stub-2)//2032
    if (cic == 1):
        a = 2036.01
        b = 0.999983
        return (stub - a)/b
    else:
        a = -2034.49
        b = 0.999412
        return (stub - a)/b


input_file = TFile("/eos/experiment/mu-e/daq/ntuples/splitted/run_0046.0.root")
tree = input_file.Get("flatTree")

corr = TH2I("correlation plot", "correlation plot", 4064,0,4064,4064,0,4064)
distance = TH1I("distance", "distance",101,-101,101)

event_id = 0
for entry in tree:
    for stub_kit, bend_code_kit in zip(entry.stub_0, entry.bend_code_0):
        if bend(ord(bend_code_kit)) == 2:
            for stub_bxl in entry.stub_1:
                corr.Fill(stub_kit,stub_bxl)
                distance.Fill(alignment(stub_kit)-stub_bxl)
    event_id +=1
    if event_id % 100000 == 0:
        print(f"{event_id}/{tree.GetEntries()} events processed ({100*event_id/tree.GetEntries():.1f}%)")

c = TCanvas("","",3000,3000)
corr.Draw("colz")
c.Print("correlation_plot.png")

distance.Draw()
c.Print("distance_plot.png")