import glob,os
output_dir="/eos/user/d/delcourt/TB_results/VCTH"
f_list = glob.glob("/eos/experiment/mu-e/daq/ntuples/split/run_0049.*.root")

os.system("rm vcth_ban.txt")

def get_file_numbering(f_name):
    f_name = f_name.split("/")[-1]
    return int(f_name.split(".")[-3]),int(f_name.split(".")[-2])

for f_index, f in enumerate(f_list):
    run = f.split("/")[-1]
    print(f_index,f,run,output_dir+"/"+run)
    print(f_index*100./len(f_list),"%")
    f_id,f_sub_id = get_file_numbering(f)
    if f_id >= 16:
        print("SKIP")
        continue
    if f_id == 17 and not f_sub_id == 1:
        print("SKIP")
        continue
    if f_id == 18 and not f_sub_id == 29:
        print("SKIP")
        continue
    if f_id == 19:
        print("SKIP")
        continue
    if f_id == 20:
        print("SKIP")
        continue
    if f_id == 21 and not f_sub_id == 30:
        print("SKIP")
        continue
    if f_id == 22 and not f_sub_id == 30:
        print("SKIP")
        continue
    if f_id == 23:
        print("SKIP")
        continue


    os.system(f"../bin/vcth_no_beam.out {f} {output_dir}/{run}")
#    os.system(f"../bin/vcth_scan.out {f} {output_dir}/{run}")
